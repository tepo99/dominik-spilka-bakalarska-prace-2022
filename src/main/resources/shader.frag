#define MAX_STEPS 1000
#define MAX_DISTANCE 1000.0
#define SURFACE_DISTANCE 0.001
#define AMBIENT 0.1;

precision highp float;

const  vec2 iResolution = vec2(1920, 1080);
uniform float iTime;
uniform float iMouseX, iMouseY;
uniform vec3 look;
uniform vec3 up;
uniform vec3 position;

const float lightSpinRadius = 0.;

/**UNIFORMS**/

vec3 transform(vec3 point, mat4 matrix) {
    return (matrix * vec4(point, 1.0)).xyz;
}

float getSphere(vec3 point, vec3 origin, float radius) {
    return length(point - origin) - radius;
}

float getCube(vec3 p, vec3 o, float size) {
    vec3 b = vec3(size, size, size);
    vec3 q = abs(p) - b;
    return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
//    return length(max(abs(point - origin) - size, 0));
}

float getCone( in vec3 p, in vec2 c, float h )
{
    // c is the sin/cos of the angle, h is height
    // Alternatively pass q instead of (c,h),
    // which is the point at the base in 2D
    vec2 q = h*vec2(c.x/c.y,-1.0);

    vec2 w = vec2( length(p.xz), p.y );
    vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
    vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
    float k = sign( q.y );
    float d = min(dot( a, a ),dot(b, b));
    float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
    return sqrt(d)*sign(s);
}

float round( float dist, float rad )
{
    return dist - rad;
}

vec3 twist(vec3 p )
{
    const float k = 2; // or some other amount
    float c = cos(k*p.y);
    float s = sin(k*p.y);
    mat2  m = mat2(c,-s,s,c);
    vec3  q = vec3(m*p.xz,p.y);
    return q;
}

vec3 bend (vec3 p )
{
    const float k = 0.2; // or some other amount
    float c = cos(k*p.x);
    float s = sin(k*p.x);
    mat2  m = mat2(c,-s,s,c);
    vec3  q = vec3(m*p.xy,p.z);
    return q;
}

float getCylinder(vec3 p, vec3 a, vec3 b, float r)
{
    vec3  ba = b - a;
    vec3  pa = p - a;
    float baba = dot(ba,ba);
    float paba = dot(pa,ba);
    float x = length(pa*baba-ba*paba) - r*baba;
    float y = abs(paba-baba*0.5)-baba*0.5;
    float x2 = x*x;
    float y2 = y*y*baba;
    float d = (max(x,y)<0.0)?-min(x2,y2):(((x>0.0)?x2:0.0)+((y>0.0)?y2:0.0));
    return sign(d)*sqrt(abs(d))/baba;
}

float getDistance(vec3 point){
    return min(min(min(/**DISTANCE_FUNCTION**/, getCylinder(point, vec3(0., 0., 0.), vec3(1., 0., 0.), 0.01)), getCylinder(point, vec3(0., 0., 0.), vec3(0., 1., 0.), 0.01)), getCylinder(point, vec3(0., 0., 0.), vec3(0., 0., 1.), 0.01));
}

//Funkce pro výpočet normály povrchu v daném bodě

vec3 getNormal(vec3 point) {
    float distance = getDistance(point);
    vec2 e = vec2(0.01, 0);

    vec3 normal = distance - vec3(
    getDistance(point-e.xyy),
    getDistance(point-e.yxy),
    getDistance(point-e.yyx)
    );
    return normalize(normal);
}

float rayMarch(vec3 rayOrigin, vec3 rayDirection) {
    float distance = 0.0;

    for(int i = 0; i< MAX_STEPS; i++ ){
        vec3 position = rayOrigin + rayDirection * distance;
        float sceneDistance = getDistance(position);
        float mod = 0.6;
        distance += min(sceneDistance * mod , MAX_DISTANCE);
        if(abs(sceneDistance) < SURFACE_DISTANCE || distance > MAX_DISTANCE) break;
    }
    return distance;
}

//Funkce jednoduchého osvětlení v daném bodě na základě úhlu normály a paprsku světla

vec4 getLight(vec3 point) {
    vec3 lightPosition = vec3(position.x, position.y + 1, position.z);
    vec3 light = normalize(lightPosition - point);
    vec3 normal = getNormal(point);

    float diffuse = clamp(dot(normal, light), 0., 1.);
    float distanceToLight = rayMarch(point + normal*SURFACE_DISTANCE*3., light);
    // Jednoduchý stín
    if(distanceToLight < length(lightPosition - point))
        diffuse *= 0.3;
    float ambient = AMBIENT;
    float lightAmount = clamp(diffuse + ambient, 0., 1.);
    vec4 color = vec4(lightAmount * 0.85, lightAmount * 0.85, lightAmount * 1.3, 1);
    return color;
}

void main()
{
    vec2 uv = ((gl_FragCoord.xy)-iResolution.xy*.5)/iResolution.y;	//normalize to resolution, 0:0 is in the center
//    vec2 uv = gl_FragCoord.xy/iResolution.xy;

    vec4 col = vec4(0, 0, 0, 1);

    vec3 f = normalize(look);
    vec3 u = normalize(up);
    vec3 r = cross(f, u);
    vec3 c=position+f*1.;
    vec3 i=c+uv.x*r+uv.y*u;
    vec3 dir=i-position;

    float d = rayMarch(position, dir);

    vec3 point = position + dir * d;

    //Ořezání maximální vzdálenosti vykreslování - bez něj způsobuje artefakty na "obloze"
    col = getLight(point);
    if(d > MAX_DISTANCE*0.9) col = vec4(vec3(0.5), 1);
    gl_FragColor = col;
}