#version 430

#define MAX_STEPS 1000
#define MAX_DISTANCE 1000.0
#define SURFACE_DISTANCE 0.001
#define AMBIENT 0.1;
#extension GL_ARB_compute_shader : enable

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout (std430, binding = 1) buffer outputBuffer
{
    float data_SSBO[];
} outBuffer;

uniform int prec;
uniform int sampleSize;
uniform int spaceOffset;
/**UNIFORMS**/

vec3 transform(vec3 point, mat4 matrix) {
    return (matrix * vec4(point, 1.0)).xyz;
}

float getSphere(vec3 point, vec3 origin, float radius) {
    return length(point - origin) - radius;
}

float getCube(vec3 p, vec3 o, float size) {
    vec3 b = vec3(size, size, size);
    vec3 q = abs(p) - b;
    return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
    //    return length(max(abs(point - origin) - size, 0));
}

float getCone( in vec3 p, in vec2 c, float h )
{
    // c is the sin/cos of the angle, h is height
    // Alternatively pass q instead of (c,h),
    // which is the point at the base in 2D
    vec2 q = h*vec2(c.x/c.y,-1.0);

    vec2 w = vec2( length(p.xz), p.y );
    vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
    vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
    float k = sign( q.y );
    float d = min(dot( a, a ),dot(b, b));
    float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
    return sqrt(d)*sign(s);
}

float round(float dist, float rad )
{
    return dist - rad;
}

vec3 twist(vec3 p )
{
    const float k = 2; // or some other amount
    float c = cos(k*p.y);
    float s = sin(k*p.y);
    mat2  m = mat2(c,-s,s,c);
    vec3  q = vec3(m*p.xz,p.y);
    return q;
}

vec3 bend (vec3 p )
{
    const float k = 0.2; // or some other amount
    float c = cos(k*p.x);
    float s = sin(k*p.x);
    mat2  m = mat2(c,-s,s,c);
    vec3  q = vec3(m*p.xy,p.z);
    return q;
}

float getCylinder(vec3 p, vec3 a, vec3 b, float r)
{
    vec3  ba = b - a;
    vec3  pa = p - a;
    float baba = dot(ba,ba);
    float paba = dot(pa,ba);
    float x = length(pa*baba-ba*paba) - r*baba;
    float y = abs(paba-baba*0.5)-baba*0.5;
    float x2 = x*x;
    float y2 = y*y*baba;
    float d = (max(x,y)<0.0)?-min(x2,y2):(((x>0.0)?x2:0.0)+((y>0.0)?y2:0.0));
    return sign(d)*sqrt(abs(d))/baba;
}

float getDistance(vec3 point){
    return round(/**DISTANCE_FUNCTION**/, 0.05);
}

void main()
{
//    vec3 point = vec3((float(gl_GlobalInvocationID.z)/5.0, float(gl_GlobalInvocationID.y)/5.0, float(gl_GlobalInvocationID.x)/5.0));
    uint offset = gl_GlobalInvocationID.z * sampleSize * sampleSize + gl_GlobalInvocationID.y * sampleSize + gl_GlobalInvocationID.x;
    float z = float(offset / (sampleSize * sampleSize))/float(prec) - spaceOffset;
    float y = float(offset % (sampleSize * sampleSize) / sampleSize)/float(prec) - spaceOffset;
    float x = float(offset % sampleSize)/float(prec) - spaceOffset;
    float dist = getDistance(vec3(x, y ,z));
    outBuffer.data_SSBO[offset] = dist;
}