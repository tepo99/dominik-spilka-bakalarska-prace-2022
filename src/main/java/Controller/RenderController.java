package Controller;


import EventListener.MouseEventListener;
import Model.Camera;
import Model.Scene;
import Model.Subscribers.SolidsListSubscriber;
import Service.Computer;
import Service.Renderer.SceneRenderer;
import Service.Renderer.UserInterfaceRenderer;
import org.joml.Vector2i;
import org.liquidengine.legui.animation.AnimatorProvider;
import org.liquidengine.legui.listener.processor.EventProcessorProvider;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.layout.LayoutManager;
import org.liquidengine.legui.system.renderer.Renderer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class RenderController implements SolidsListSubscriber {
    private static final int WIDTH = 1920, HEIGHT = 1080;
    // The window handle
    private long window;
    private SceneRenderer sceneRenderer;
    private UserInterfaceRenderer uiRenderer;

    public void start() {
        if (!GLFW.glfwInit()) {
            throw new RuntimeException("Can't initialize GLFW");
        }
        window = createWindow();
        Scene scene = new Scene();
        Camera camera = new Camera();
        MouseEventListener mouseEventListener = new MouseEventListener(camera, window, scene);
        uiRenderer = new UserInterfaceRenderer(scene, camera, window, mouseEventListener);
        uiRenderer.getSolidsList().subscribe(this);
        sceneRenderer = new SceneRenderer(scene, camera, mouseEventListener);
        loop();

        // And when rendering is ended we need to destroy renderer
        sceneRenderer.destroy();
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    private long createWindow() {
        long window = glfwCreateWindow(WIDTH, HEIGHT, "Single Class Example", NULL, NULL);
        glfwShowWindow(window);

        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        glfwSwapInterval(0);
        return window;
    }


    private void loop() {
        Renderer renderer = uiRenderer.getInitializer().getRenderer();
        Context context = uiRenderer.getInitializer().getContext();
        while (uiRenderer.isRunning()) {
            context.updateGlfwWindow();
            Vector2i windowSize = context.getFramebufferSize();

            glClearColor(1, 1, 1, 1);
            // Set viewport size
            glViewport(0, 0, windowSize.x, windowSize.y);
            // Clear screen
            glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            glEnable(GL_BLEND);
            glDisable(GL_DEPTH_TEST);

            // render custom frame
            sceneRenderer.renderCustomFrame();

            // render gui frame
            if (!uiRenderer.isHiding()) {
                renderer.render(uiRenderer.getFrame(), context);
            }

            // poll events to callbacks
            glfwPollEvents();
            glfwSwapBuffers(window);

            // Now we need to process events. Firstly we need to process system events.
            uiRenderer.getInitializer().getSystemEventProcessor().processEvents(uiRenderer.getFrame(), context);

            // When system events are translated to GUI events we need to process them.
            // This event processor calls listeners added to ui components
            EventProcessorProvider.getInstance().processEvents();

            // When everything done we need to relayout components.
            LayoutManager.getInstance().layout(uiRenderer.getFrame());

            // Run animations. Should be also called cause some components use animations for updating state.
            AnimatorProvider.getAnimator().runAnimations();
        }
    }


    public void recalculate() {
        sceneRenderer.initializeCustomFrame();
        uiRenderer.generateOnFly();
    }

    @Override
    public void react() {
        recalculate();
    }
}
