package Service.Saving;

import Factory.Abstract.ModifierFactory;
import Factory.Abstract.OperatorFactory;
import Factory.Abstract.SolidFactory;
import Model.Scene;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

public class XmlLoad {

    private int elementCount = 0;

    public void loadScene(Scene scene, String file) throws IOException, ParserConfigurationException, SAXException, DataFormatException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        Element rootNode = document.getDocumentElement();
        Model.Element.Element root = getElement(rootNode.getFirstChild());
        scene.setRoot(root);
        scene.setElementCount(elementCount);
    }

    private Model.Element.Element getElement(Node node) throws DataFormatException {
        Model.Element.Element element = null;
        String name = ((Element)node).getElementsByTagName("Name").item(0).getFirstChild().getNodeValue();
        String function = ((Element)node).getElementsByTagName("Function").item(0).getFirstChild().getNodeValue();
        switch (node.getAttributes().getNamedItem("type").getNodeValue()) {
            case "Operator" -> element = OperatorFactory.buildFromInfo(name, function);
            case "Modifier" -> element = ModifierFactory.buildFromInfo(name, function);
            case "Solid" -> element = SolidFactory.buildFromInfo(name, function);
        }
        if(null == element) {
            throw new DataFormatException("Unable to process XML file.");
        }
        NodeList transforms = ((Element)node).getElementsByTagName("Transforms").item(0).getChildNodes();
        for(int i = 0; i < transforms.getLength(); i++) {
            Node transform = transforms.item(i);
            switch (transform.getAttributes().getNamedItem("type").getNodeValue()){
                case "translate" -> setTranslation(element, transform);
                case "rotate" -> setRotation(element, transform);
                case "scale" -> setScale(element, transform);
            }
        }
        if(!node.getAttributes().getNamedItem("type").getNodeValue().equals("Solid")){
            List<Model.Element.Element> children = new ArrayList<>();
            NodeList childNodes = ((Element) node).getElementsByTagName("Children").item(0).getChildNodes();
            for(int i = 0; i < childNodes.getLength(); i++) {
                Model.Element.Element child = getElement(childNodes.item(i));
                child.setParent(element);
                children.add(child);
            }
            element.setChildren(children);
        }
        elementCount++;
        return element;
    }

    private void setTranslation(Model.Element.Element element, Node transform) {
        switch(transform.getAttributes().getNamedItem("direction").getNodeValue()) {
            case "x" -> element.setTranslationx(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "y" -> element.setTranslationy(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "z" -> element.setTranslationz(Float.parseFloat(transform.getFirstChild().getNodeValue()));
        }
    }

    private void setRotation(Model.Element.Element element, Node transform) {
        switch(transform.getAttributes().getNamedItem("direction").getNodeValue()) {
            case "x" -> element.setRotx(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "y" -> element.setRoty(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "z" -> element.setRotz(Float.parseFloat(transform.getFirstChild().getNodeValue()));
        }
    }

    private void setScale(Model.Element.Element element, Node transform) {
        switch(transform.getAttributes().getNamedItem("direction").getNodeValue()) {
            case "x" -> element.setScalex(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "y" -> element.setScaley(Float.parseFloat(transform.getFirstChild().getNodeValue()));
            case "z" -> element.setScalez(Float.parseFloat(transform.getFirstChild().getNodeValue()));
        }
    }
}
