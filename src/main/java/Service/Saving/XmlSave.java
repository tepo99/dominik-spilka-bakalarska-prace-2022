package Service.Saving;

import Model.Element.Element;
import Model.Element.Modifier;
import Model.Element.Operator;
import Model.Element.Solid;
import Model.Helper.Parameter;
import Model.Helper.Vec3;
import Model.Helper.Vec4;
import Model.Scene;
import Service.MatrixParser;
import org.apache.commons.io.output.XmlStreamWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class XmlSave {

    public void saveScene(Scene scene, String file) throws IOException {
        FileWriter writer = new FileWriter(file);
        Element root = scene.getRoot();
        writer.append("<Scene>");
        writeElement(root, writer);
        writer.append("</Scene>");
        writer.close();
    }

    private void writeElement(Element element, FileWriter writer) throws IOException {
        String s = "";
        if(element instanceof Solid) {
            s = "Solid";
        }
        if(element instanceof Operator) {
            s = "Operator";
        }
        if(element instanceof Modifier) {
            s = "Modifier";
        }
        writer.append(String.format("<Element type='%s'>", s));
        writer.append(String.format("<Name>%s</Name>", element.getName()));
        writer.append(String.format("<Function>%s</Function>", element.getFunctionPure()));
        writer.append("<Transforms>");
        writer.append(String.format("<Transform type='translate' direction='x'>%s</Transform>", element.getTranslationx()));
        writer.append(String.format("<Transform type='translate' direction='y'>%s</Transform>", element.getTranslationy()));
        writer.append(String.format("<Transform type='translate' direction='z'>%s</Transform>", element.getTranslationz()));
        writer.append(String.format("<Transform type='rotate' direction='x'>%s</Transform>", element.getRotx()));
        writer.append(String.format("<Transform type='rotate' direction='y'>%s</Transform>", element.getRoty()));
        writer.append(String.format("<Transform type='rotate' direction='z'>%s</Transform>", element.getRotz()));
        writer.append(String.format("<Transform type='scale' direction='x'>%s</Transform>", element.getScalex()));
        writer.append(String.format("<Transform type='scale' direction='y'>%s</Transform>", element.getScaley()));
        writer.append(String.format("<Transform type='scale' direction='z'>%s</Transform>", element.getScalez()));
        writer.append("</Transforms>");
        if(element instanceof Operator || element instanceof Modifier) {
            writer.append("<Children>");
            for(Element child: element.getChildren()){
                writeElement(child, writer);
            }
            writer.append("</Children>");
        }
        writer.append("</Element>");
    }
}
