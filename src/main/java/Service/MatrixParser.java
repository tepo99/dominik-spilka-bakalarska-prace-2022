package Service;

import org.joml.Matrix4f;

public class MatrixParser {

    public static String parse(Matrix4f mat) {
        return mat.m00() + "," +
                mat.m01() + "," +
                mat.m02() + "," +
                mat.m03() + "," +
                mat.m10() + "," +
                mat.m11() + "," +
                mat.m12() + "," +
                mat.m13() + "," +
                mat.m20() + "," +
                mat.m21() + "," +
                mat.m22() + "," +
                mat.m23() + "," +
                mat.m30() + "," +
                mat.m31() + "," +
                mat.m32() + "," +
                mat.m33();
    }

    public static Matrix4f getTranslation(double x, double y, double z) {
        Matrix4f mat = new Matrix4f();
        mat.m30((float) x);
        mat.m31((float) y);
        mat.m32((float) z);
        return mat;
    }

    public static Matrix4f getRotX(double alpha){
        Matrix4f mat = new Matrix4f().identity();
        mat.m11((float) Math.cos(alpha));
        mat.m22((float) Math.cos(alpha));
        mat.m21((float) -Math.sin(alpha));
        mat.m12((float) Math.sin(alpha));
        return mat;
    }

    public static Matrix4f getRot(double alhpa, double beta, double gamma) {
        return getRotX(alhpa).mul(getRotY(beta)).mul(getRotZ(gamma));
    }

    public static Matrix4f getRotY(double alpha) {
        Matrix4f mat = new Matrix4f().identity();
        mat.m00((float) Math.cos(alpha));
        mat.m22((float) Math.cos(alpha));
        mat.m20((float) Math.sin(alpha));
        mat.m02((float) -Math.sin(alpha));
        return mat;
    }

    public static Matrix4f getRotZ(double alpha) {
        Matrix4f mat = new Matrix4f().identity();
        mat.m00((float) Math.cos(alpha));
        mat.m11((float) Math.cos(alpha));
        mat.m10((float) -Math.sin(alpha));
        mat.m01((float) Math.sin(alpha));
        return mat;
    }

    public static Matrix4f getScale(double x, double y, double z) {
        Matrix4f mat = new Matrix4f();
        mat.m00((float) x);
        mat.m11((float) y);
        mat.m22((float) z);
        return mat;
    }

    public static float[] matToArray(Matrix4f mat) {
        return new float[]{mat.m00(),
                mat.m01(),
                mat.m02(),
                mat.m03(),
                mat.m10(),
                mat.m11(),
                mat.m12(),
                mat.m13(),
                mat.m20(),
                mat.m21(),
                mat.m22(),
                mat.m23(),
                mat.m30(),
                mat.m31(),
                mat.m32(),
                mat.m33()
                };
    }
}
