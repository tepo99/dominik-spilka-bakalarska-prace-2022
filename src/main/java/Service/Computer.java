package Service;

import Model.Element.Element;
import Model.Helper.Vec3;
import Model.Scene;
import Service.Shader.Shader;
import com.google.common.base.Charsets;
import org.apache.commons.io.FileUtils;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL43;

import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.*;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30C.glBindBufferBase;
import static org.lwjgl.opengl.GL42C.GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT;
import static org.lwjgl.opengl.GL43.*;
import static org.lwjgl.opengl.GL43C.GL_SHADER_STORAGE_BUFFER;
import static org.lwjgl.opengl.GL45.glCreateBuffers;

public class Computer {


    private Scene scene;

    private int shaderProgram;
    private int framebufferImageBinding;
    private int precision = 12;
    private int sampleSize = 200;
    private int offset = 5;

    private int tex;
    private float[] data;
    public static final double SURFACE_DIST = 0.00001;

    public Computer(Scene scene) {
        this.scene = scene;
    }

    //@formatter:on
    public void initializeCustomFrame() {
        Shader shader = new Shader(scene);
        shaderProgram = glCreateProgram();
        int computeShader = glCreateShader(GL43.GL_COMPUTE_SHADER);
        glShaderSource(computeShader, shader.getComputeShaderSource());
        glCompileShader(computeShader);
        if (glGetShaderi(computeShader, GL_COMPILE_STATUS) == GL_FALSE) {
            System.out.println(shader.getFragmentShaderSource());
            System.out.println(shader.getComputeShaderSource());
            throw new RuntimeException("Error creating compute shader\n"
                    + glGetShaderInfoLog(computeShader, glGetShaderi(computeShader, GL_INFO_LOG_LENGTH)));
        }

        // Attach the shader
        glAttachShader(shaderProgram, computeShader);

        // Link this program
        glLinkProgram(shaderProgram);

        // Check for linking errors
        if (glGetProgrami(shaderProgram, GL_LINK_STATUS) == GL_FALSE)
            throw new RuntimeException("Unable to link shader program:\n" + glGetProgramInfoLog(shaderProgram, glGetProgrami(shaderProgram, GL_INFO_LOG_LENGTH)));

        glUseProgram(shaderProgram);
    }

    public void renderCustomFrame(File file) throws IOException {
        int ssbo = glCreateBuffers();
        IntBuffer buffer = BufferUtils.createIntBuffer(sampleSize * sampleSize * sampleSize);
        for (int i = 0; i < sampleSize * sampleSize * sampleSize; i++) {
            buffer.put(0);
        }
        buffer.flip();

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
        glBufferData(GL_SHADER_STORAGE_BUFFER, buffer, GL_DYNAMIC_READ);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

        int blockIndex = glGetProgramResourceIndex(shaderProgram, GL_SHADER_STORAGE_BLOCK, "outBuffer");

        int ssboBindingPointIndex = 1;
        glShaderStorageBlockBinding(shaderProgram, blockIndex, ssboBindingPointIndex);

        int bindingPointIndex = 1;
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPointIndex, ssbo);

        glUseProgram(shaderProgram);

        int precisionP = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "prec");
        int sampleSizeP = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "sampleSize");
        int offsetP = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "spaceOffset");
        ARBShaderObjects.glUniform1iARB(precisionP, precision);
        ARBShaderObjects.glUniform1iARB(sampleSizeP, sampleSize);
        ARBShaderObjects.glUniform1iARB(offsetP, offset);

        List<Element> elements = new ArrayList<>();
        scene.getElements(scene.getRoot(), elements);
        for (Element e : elements) {
            int pointer = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, e.getTransformShaderName());
            ARBShaderObjects.glUniformMatrix4fvARB(pointer, false, MatrixParser.matToArray(e.getTransform()));
        }

        GL43.glDispatchCompute(sampleSize, sampleSize, sampleSize);
        GL43.glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);

        float[] data = new float[sampleSize * sampleSize * sampleSize];
        glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, data);

        this.data = data;
        buildScene(file);
    }

    public void buildScene(File file) throws IOException {
        int max = sampleSize * sampleSize * sampleSize;
        int squareSize = sampleSize * sampleSize;
        HashMap<String, String> vertices = new HashMap<>();
        HashMap<Integer, String[]> faces = new HashMap<>();
        int faceCount = 0;
        System.out.println("Max: " + max + " ; data.length: " + data.length);
        for (int i = 0; i < max; i++) {

            //FIXME overflows from space edges
            int c0 = i;
            int c1 = i + 1;
            int c2 = i + sampleSize + 1;
            int c3 = i + sampleSize;
            int c4 = i + squareSize;
            int c5 = i + squareSize + 1;
            int c6 = i + squareSize + sampleSize + 1;
            int c7 = i + squareSize + sampleSize;
            //end
            if (c1 >= max || c2 >= max || c3 >= max || c4 >= max || c5 >= max || c6 >= max || c7 >= max) {
                break;
            }

            //empty
            if (
                    data[c0] > SURFACE_DIST &&
                            data[c1] > SURFACE_DIST &&
                            data[c2] > SURFACE_DIST &&
                            data[c3] > SURFACE_DIST &&
                            data[c4] > SURFACE_DIST &&
                            data[c5] > SURFACE_DIST &&
                            data[c6] > SURFACE_DIST &&
                            data[c7] > SURFACE_DIST
            ) {
                continue;
            }
            //full
            if (data[c0] < SURFACE_DIST &&
                    data[c1] < SURFACE_DIST &&
                    data[c2] < SURFACE_DIST &&
                    data[c3] < SURFACE_DIST &&
                    data[c4] < SURFACE_DIST &&
                    data[c5] < SURFACE_DIST &&
                    data[c6] < SURFACE_DIST &&
                    data[c7] < SURFACE_DIST) {
                continue;
            }
            int corners = (data[c0] < SURFACE_DIST ? 1 : 0) +
                    (data[c1] < SURFACE_DIST ? 1 : 0) +
                    (data[c2] < SURFACE_DIST ? 1 : 0) +
                    (data[c3] < SURFACE_DIST ? 1 : 0) +
                    (data[c4] < SURFACE_DIST ? 1 : 0) +
                    (data[c5] < SURFACE_DIST ? 1 : 0) +
                    (data[c6] < SURFACE_DIST ? 1 : 0) +
                    (data[c7] < SURFACE_DIST ? 1 : 0);
            if (corners == 1) {
                faceCount = fillFromSingle(vertices, faces, faceCount, c0, c1, c3, c4);
                faceCount = fillFromSingle(vertices, faces, faceCount, c1, c0, c2, c5);
                faceCount = fillFromSingle(vertices, faces, faceCount, c2, c1, c3, c6);
                faceCount = fillFromSingle(vertices, faces, faceCount, c3, c2, c0, c7);
                faceCount = fillFromSingle(vertices, faces, faceCount, c4, c5, c7, c0);
                faceCount = fillFromSingle(vertices, faces, faceCount, c5, c4, c6, c1);
                faceCount = fillFromSingle(vertices, faces, faceCount, c6, c5, c7, c2);
                faceCount = fillFromSingle(vertices, faces, faceCount, c7, c4, c6, c3);
            }
            if (corners == 2) {
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c0, c1, c4, c3, c5, c2);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c1, c2, c5, c0, c6, c3);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c2, c3, c6, c1, c7, c0);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c3, c0, c7, c2, c4, c1);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c4, c5, c0, c7, c1, c6);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c5, c6, c1, c4, c2, c7);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c6, c7, c2, c5, c3, c4);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c7, c4, c3, c6, c0, c5);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c0, c4, c1, c3, c5, c7);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c1, c5, c2, c0, c6, c4);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c2, c6, c3, c1, c7, c5);
                faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, c3, c7, c0, c2, c4, c6);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c0, c5, c1, c4, c3, c6);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c1, c4, c0, c5, c2, c7);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c1, c6, c2, c5, c0, c7);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c2, c5, c1, c6, c3, c4);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c2, c7, c3, c6, c1, c4);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c3, c6, c2, c7, c0, c5);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c3, c4, c0, c7, c2, c5);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c0, c7, c4, c3, c1, c6);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c0, c2, c1, c3, c4, c6);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c1, c3, c0, c2, c5, c7);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c4, c6, c5, c7, c0, c2);
                faceCount = fillFromDoubleDiagonal(vertices, faces, faceCount, c5, c7, c4, c6, c1, c3);
                faceCount = fillFromDoubleTriagonal(vertices, faces, faceCount, c0, c6, c1, c3, c4, c5, c7, c2);
                faceCount = fillFromDoubleTriagonal(vertices, faces, faceCount, c1, c7, c2, c0, c5, c4, c6, c3);
                faceCount = fillFromDoubleTriagonal(vertices, faces, faceCount, c2, c4, c3, c1, c6, c5, c7, c0);
                faceCount = fillFromDoubleTriagonal(vertices, faces, faceCount, c3, c5, c0, c2, c7, c6, c4, c1);
            }
            if (corners == 3) {
                //floor
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c0, c1, c2, c3, c4, c5, c6);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c1, c2, c3, c0, c5, c6, c7);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c2, c3, c0, c1, c6, c7, c4);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c3, c0, c1, c2, c7, c4, c5);
                //ceil
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c4, c5, c6, c7, c0, c1, c2);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c5, c6, c7, c4, c1, c2, c3);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c6, c7, c4, c5, c2, c3, c0);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c7, c4, c5, c6, c3, c0, c1);
                //right to up
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c0, c1, c5, c4, c3, c2,c6);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c1, c2, c6, c5, c0, c3, c7);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c2, c3, c7, c6, c1, c0, c4);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c3, c0, c4, c7, c2, c1,c5);
                //down to right
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c4, c0, c1, c5, c7, c3,c2);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c5, c1, c2, c6, c4, c0,c3);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c6, c2, c3, c7, c5, c1,c0);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c7, c3, c0, c4, c6, c2,c1);
                //left to down
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c5, c4, c0, c1, c6, c7,c3);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c6, c5, c1, c2, c7, c4,c0);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c7, c6, c2, c3, c4, c5,c1);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c4, c7, c3, c0, c5, c6,c2);
                //up to left
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c1, c5, c4, c0, c2, c6,c7);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c2, c6, c5, c1, c3, c7,c4);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c3, c7, c6, c2, c0, c4,c5);
                faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, c0, c4, c7, c3, c1, c5,c6);
                //double with corner accross
                //floor - top right corner
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c0, c1, c6, c4, c3, c5, c2, c7);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c1, c2, c7, c5, c0, c6, c3, c4);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c2, c3, c4, c6, c1, c7, c0, c5);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c3, c0, c5, c7, c2, c4, c1, c6);
                //ceil - top left corner
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c1, c0, c7, c5, c2, c4, c3, c6);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c2, c1, c4, c6, c3, c5, c0, c7);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c3, c2, c5, c7, c0, c6, c1, c4);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c0, c3, c6, c4, c1, c7, c2, c5);
                //ceil - top right corner
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c5, c4, c3, c1, c6, c0, c7, c2);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c6, c5, c0, c2, c7, c1, c4, c3);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c7, c6, c1, c3, c4, c2, c5, c0);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c4, c7, c2, c0, c5, c3, c6, c1);
                //ceil - top left corner
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c4, c5, c2, c0, c7, c1, c6, c3);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c5, c6, c3, c1, c4, c2, c7, c0);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c6, c7, c0, c2, c5, c3, c4, c1);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c7, c4, c1, c3, c6, c0, c5, c2);
                //sides
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c0, c4, c6, c1, c3, c5, c7, c2);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c1, c5, c7, c2, c0, c6, c4, c3);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c2, c6, c4, c3, c1, c7, c5, c0);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c3, c7, c5, c0, c2, c4, c6, c1);

                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c4, c0, c2, c5, c7, c1, c3, c6);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c5, c1, c3, c6, c4, c2, c0, c7);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c6, c2, c0, c7, c5, c3, c1, c4);
                faceCount = fillFromTripleTriagonal(vertices, faces, faceCount, c7, c3, c1, c4, c6, c0, c2, c5);

                faceCount = fillFromTripleDisjoint(vertices, faces, faceCount, c1, c4, c6, c0, c2, c5, c7);
                faceCount = fillFromTripleDisjoint(vertices, faces, faceCount, c2, c5, c7, c1, c3, c6, c4);
                faceCount = fillFromTripleDisjoint(vertices, faces, faceCount, c3, c6, c4, c2, c0, c7, c5);
                faceCount = fillFromTripleDisjoint(vertices, faces, faceCount, c0, c7, c5, c3, c1, c4, c6);

            }
            if (corners == 4) {
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c0, c1, c2, c3, c4, c5, c6, c7);
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c4, c5, c6, c7, c0, c1, c2, c3);
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c0, c1, c5, c4, c3, c2, c6, c7);
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c1, c2, c6, c5, c0, c3, c7, c4);
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c2, c3, c7, c6, c1, c0, c4, c5);
                faceCount = fillFromQuadFace(vertices, faces, faceCount, c3, c0, c4, c7, c2, c1, c5, c6);
                //floor
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c3, c0, c2, c7, c1, c6, c4);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c0, c1, c3, c4, c2, c7, c5);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c1, c2, c0, c5, c3, c4, c6);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c2, c3, c1, c6, c0, c5, c7);
                //ceiling
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c7, c6, c4, c3, c5, c0, c2);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c4, c5, c7, c0, c6, c3, c1);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c5, c6, c4, c1, c7, c0, c2);
                faceCount = fillFromQuadCorner(vertices, faces, faceCount, c6, c7, c5, c2, c4, c1, c3);

                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c0, c4, c2, c6, c1, c5, c3, c7);
                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c1, c5, c3, c7, c2, c6, c0, c4);
                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c0, c1, c7, c6, c3, c2, c4, c5);
                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c2, c3, c5, c4, c1, c0, c6, c7);
                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c1, c2, c4, c7, c0, c3, c5, c6);
                faceCount = fillFromQuadOpposite(vertices, faces, faceCount, c0, c3, c5, c6, c1, c2, c4, c7);

                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c0, c3, c2, c6, c1, c4, c7, c5);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c1, c0, c3, c7, c2, c5, c4, c6);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c2, c1, c0, c4, c3, c6, c5, c7);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c3, c2, c1, c5, c0, c7, c6, c4);

                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c5, c4, c7, c3, c6, c1, c2, c0);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c6, c5, c4, c0, c7, c2, c3, c1);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c7, c6, c5, c1, c4, c3, c0, c2);
                faceCount = fillFromQuadZigZag(vertices, faces, faceCount, c4, c7, c6, c2, c5, c0, c1, c3);
                //todo could have more combinations possibly, needs further testing

                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c1, c2, c3, c4, c0, c5, c6,c7);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c2, c3, c0, c5, c1, c6, c7,c4);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c3, c0, c1, c6, c2, c7, c4,c5);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c0, c1, c2, c7, c3, c4, c5,c6);

                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c5, c6, c7, c0, c4, c1, c2,c3);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c6, c7, c4, c1, c5, c2, c3,c0);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c7, c4, c5, c2, c6, c3, c0,c1);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c4, c5, c6, c3, c7, c0, c1,c2);

                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c0, c3, c7, c5, c4, c1, c2,c6);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c1, c0, c4, c6, c5, c2, c3,c7);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c2, c1, c5, c7, c6, c3, c0,c4);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c3, c2, c6, c4, c7, c0, c1,c5);

                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c5, c6, c2, c0, c1, c4, c7,c3);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c6, c7, c3, c1, c2, c5, c4,c0);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c7, c4, c0, c2, c3, c6, c5,c1);
                faceCount = fillFromTripleSingle(vertices, faces, faceCount, c4, c5, c1, c3, c0, c7, c6,c2);

                faceCount = fillFromQuadDisjoint(vertices, faces, faceCount, c0, c2, c5, c7, c1, c3, c4, c6);
                faceCount = fillFromQuadDisjoint(vertices, faces, faceCount, c1, c3, c4, c6, c0, c2, c5, c7);
            }
            if(corners == 5) {
                //floor
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c2, c3, c7, c4, c5, c1, c0, c6);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c3, c0, c4, c5, c6, c2, c1, c7);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c0, c1, c5, c6, c7, c3, c2, c4);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c1, c2, c6, c7, c4, c0, c3, c5);
                //ceil
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c7, c6, c2, c1, c0, c4, c5, c3);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c4, c7, c3, c2, c1, c5, c6, c0);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c5, c4, c0, c3, c2, c6, c7, c1);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c6, c5, c1, c0, c3, c7, c4, c2);
                //left 90
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c4, c0, c3, c2, c6, c5, c1, c7);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c5, c1, c0, c3, c7, c6, c2, c4);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c6, c2, c1, c0, c4, c7, c3, c5);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c7, c3, c2, c1, c5, c4, c0, c6);
                //right 90
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c1, c5, c6, c7, c3, c0, c4, c2);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c2, c6, c7, c4, c0, c1, c5, c3);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c3, c7, c4, c5, c1, c2, c6, c0);
                faceCount = fillFromQuintDoubleSingle(vertices, faces, faceCount, c0, c4, c5, c6, c2, c3, c7, c1);

                //floor
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c0, c4, c5, c6, c7, c1, c2, c3);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c1, c5, c6, c7, c4, c2, c3, c0);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c2, c6, c7, c4, c5, c3, c0, c1);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c3, c7, c4, c5, c6, c0, c1, c2);
                //ceil
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c5, c1, c0, c3, c2, c4, c7, c6);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c6, c2, c1, c0, c3, c5, c4, c7);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c7, c3, c2, c1, c0, c6, c5, c4);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c4, c0, c3, c2, c1, c7, c6, c5);
                //left 90
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c1, c0, c4, c7, c3, c5, c6, c2);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c2, c1, c5, c4, c0, c6, c7, c3);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c3, c2, c6, c5, c1, c7, c4, c0);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c0, c3, c7, c6, c2, c4, c5, c1);
                //right 90
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c4, c5, c1, c2, c6, c0, c3, c7);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c5, c6, c2, c3, c7, c1, c0, c4);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c6, c7, c3, c0, c4, c2, c1, c5);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c7, c4, c0, c1, c5, c3, c2, c6);

                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c2, c3, c7, c4, c0, c6, c5, c1);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c3, c0, c4, c5, c1, c7, c6, c2);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c0, c1, c5, c6, c2, c4, c7, c3);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c1, c2, c6, c7, c3, c5, c4, c0);

                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c5, c4, c7, c3, c0, c6, c2, c1);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c6, c5, c4, c0, c1, c7, c3, c2);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c7, c6, c5, c1, c2, c4, c0, c3);
                faceCount = fillFromQuintCorner(vertices, faces, faceCount, c4, c7, c6, c2, c3, c5, c1, c0);
            }
            if(corners == 6) {
                faceCount = fillFromHexOpposite(vertices, faces, faceCount, c1, c2, c3, c4, c5, c7, c0, c6);
                faceCount = fillFromHexOpposite(vertices, faces, faceCount, c2, c3, c0, c5, c6, c4, c1, c7);
                faceCount = fillFromHexOpposite(vertices, faces, faceCount, c3, c0, c1, c6, c7, c5, c2, c4);
                faceCount = fillFromHexOpposite(vertices, faces, faceCount, c0, c1, c2, c7, c4, c6, c3, c5);

                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c1, c2, c3, c6, c7, c4, c0, c5);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c2, c3, c0, c7, c4, c5, c1, c6);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c3, c0, c1, c4, c5, c6, c2, c7);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c0, c1, c2, c5, c6, c7, c3, c4);

                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c5, c6, c2, c7, c3, c0, c1, c4);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c6, c7, c3, c4, c0, c1, c2, c5);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c7, c4, c0, c5, c1, c2, c3, c6);
                faceCount = fillFromHexDiagonal(vertices, faces, faceCount, c4, c5, c1, c6, c2, c3, c0, c7);

                faceCount = fillFromHexCorner(vertices, faces, faceCount, c2, c3, c4, c5, c6, c7, c0, c1);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c3, c0, c5, c6, c7, c4, c1, c2);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c0, c1, c6, c7, c4, c5, c2, c3);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c1, c2, c7, c4, c5, c6, c3, c0);

                faceCount = fillFromHexCorner(vertices, faces, faceCount, c7, c6, c1, c0, c3, c2, c5, c4);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c4, c7, c2, c1, c0, c3, c6, c5);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c5, c4, c3, c2, c1, c0, c7, c6);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c6, c5, c0, c3, c2, c1, c4, c7);

                faceCount = fillFromHexCorner(vertices, faces, faceCount, c6, c2, c0, c4, c7, c3, c1, c5);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c7, c3, c1, c5, c4, c0, c2, c6);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c4, c0, c2, c6, c5, c1, c3, c7);
                faceCount = fillFromHexCorner(vertices, faces, faceCount, c5, c1, c3, c7, c6, c2, c0, c4);
            }
            if(corners == 7) {
                faceCount = fillFromSeven(vertices, faces, faceCount, c1, c2, c3, c4, c5, c6, c7, c0);
                faceCount = fillFromSeven(vertices, faces, faceCount, c2, c3, c0, c5, c6, c7, c4, c1);
                faceCount = fillFromSeven(vertices, faces, faceCount, c3, c0, c1, c6, c7, c4, c5, c2);
                faceCount = fillFromSeven(vertices, faces, faceCount, c0, c1, c2, c7, c4, c5, c6, c3);

                faceCount = fillFromSeven(vertices, faces, faceCount, c5, c6, c7, c0, c1, c2, c3, c4);
                faceCount = fillFromSeven(vertices, faces, faceCount, c6, c7, c4, c1, c2, c3, c0, c5);
                faceCount = fillFromSeven(vertices, faces, faceCount, c7, c4, c5, c2, c3, c0, c1, c6);
                faceCount = fillFromSeven(vertices, faces, faceCount, c4, c5, c6, c3, c0, c1, c2, c7);
            }
        }
        StringBuilder vsb = new StringBuilder();
        HashMap<String, Integer> indexes = new HashMap<>();
        int index = 1;
        for (Map.Entry<String, String> entry : vertices.entrySet()) {
            vsb.append(entry.getValue());
            if (!indexes.containsKey(entry.getKey())) {
                indexes.put(entry.getKey(), index++);
            }
        }
        StringBuilder fsb = new StringBuilder();
        for (Map.Entry<Integer, String[]> entry : faces.entrySet()) {
            String[] value = entry.getValue();
            fsb.append(String.format("f %s %s %s\n", indexes.get(value[0]).toString(), indexes.get(value[1]).toString(), indexes.get(value[2]).toString()));
        }
        String data = "o Cube\n";
        data += vsb.toString();
        data += fsb.toString();

        FileUtils.writeStringToFile(file, data, Charsets.UTF_8);
    }

    private int fillFromSingle(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p, int n1, int n2, int n3) {
        if (data[p] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p, n1);
            Vec3 v1 = getMidpoint(p, n2);
            Vec3 v2 = getMidpoint(p, n3);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
        }
        return faceCount;
    }

    private int fillFromDoubleNeighbor(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int n11, int n12, int n21, int n22) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n11);
            Vec3 v1 = getMidpoint(p1, n12);
            Vec3 v2 = getMidpoint(p2, n21);
            Vec3 v3 = getMidpoint(p2, n22);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v2.toString(), v3.toString()});
        }
        return faceCount;
    }

    private int fillFromDoubleDiagonal(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int n1, int n2, int no1, int no2) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST) {
            faceCount = fillFromSingle(vertices, faces, faceCount, p1, n1, n2, no1);
            faceCount = fillFromSingle(vertices, faces, faceCount, p2, n1, n2, no2);
        }
        return faceCount;
    }

    private int fillFromDoubleTriagonal(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int n11, int n12, int n13, int n21, int n22, int n23) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST) {
            faceCount = fillFromSingle(vertices, faces, faceCount, p1, n11, n12, n13);
            faceCount = fillFromSingle(vertices, faces, faceCount, p2, n21, n22, n23);
        }
        return faceCount;
    }

    //c0, c1, c2, c3, c4, c6
    //p1, p2, p3, n11, n12, n32
    private int fillFromTripleNeighbor(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int n11, int n12, int n22, int n32) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n11);
            Vec3 v1 = getMidpoint(p1, n12);
            Vec3 v2 = getMidpoint(p3, n11);
            Vec3 v3 = getMidpoint(p3, n32);
            Vec3 v4 = getMidpoint(p2, n22);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v2.toString(), v3.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v3.toString(), v4.toString()});
        }
        return faceCount;
    }

    private int fillFromTripleTriagonal(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int n11, int n12, int n21, int n22, int n31) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST) {
            faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, p1, p2, n11, n12, n21, n22);
            faceCount = fillFromSingle(vertices, faces, faceCount, p3, n31, n21, n22);
        }
        return faceCount;
    }

    private int fillFromTripleDisjoint(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int n12, int n13, int n123, int n23) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST) {
            faceCount = fillFromSingle(vertices, faces, faceCount, p1, n12, n13, n123);
            faceCount = fillFromSingle(vertices, faces, faceCount, p2, n12, n123, n23);
            faceCount = fillFromSingle(vertices, faces, faceCount, p1, n13, n123, n23);
        }
        return faceCount;
    }

    private int fillFromQuadFace(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int n1, int n2, int n3, int n4) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p2, n2);
            Vec3 v2 = getMidpoint(p3, n3);
            Vec3 v3 = getMidpoint(p4, n4);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v0.toString(), v2.toString(), v3.toString()});
        }
        return faceCount;
    }

    //c0, c1, c3, c4, c2, c7, c5
    private int fillFromQuadCorner(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p0, int p1, int p2, int p3, int n12, int n23, int n31) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p0] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n12);
            Vec3 v1 = getMidpoint(p2, n12);
            Vec3 v2 = getMidpoint(p2, n23);
            Vec3 v3 = getMidpoint(p1, n31);
            Vec3 v4 = getMidpoint(p3, n23);
            Vec3 v5 = getMidpoint(p3, n31);

            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            vertices.put(v5.toString(), String.format("v %s %s %s\n", v5.getX(), v5.getY(), v5.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v0.toString(), v2.toString(), v3.toString()});
            faces.put(faceCount++, new String[]{v2.toString(), v3.toString(), v4.toString()});
            faces.put(faceCount++, new String[]{v4.toString(), v5.toString(), v3.toString()});

        }
        return faceCount;
    }


    private int fillFromQuadOpposite(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int n131, int n241, int n132, int n242) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST) {
            faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, p1, p2, n131, n132, n241, n242);
            faceCount = fillFromDoubleNeighbor(vertices, faces, faceCount, p3, p4, n131, n132, n241, n242);
        }
        return faceCount;
    }

    //c0, c3, c2, c6, c1, c4, c7, c5
    private int fillFromQuadZigZag(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int n1, int n2, int n3, int n4) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p1, n2);
            Vec3 v2 = getMidpoint(p2, n3);
            Vec3 v3 = getMidpoint(p3, n1);
            Vec3 v4 = getMidpoint(p4, n3);
            Vec3 v5 = getMidpoint(p4, n4);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            vertices.put(v5.toString(), String.format("v %s %s %s\n", v5.getX(), v5.getY(), v5.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v0.toString(), v2.toString(), v5.toString()});
            faces.put(faceCount++, new String[]{v0.toString(), v5.toString(), v3.toString()});
            faces.put(faceCount++, new String[]{v2.toString(), v5.toString(), v4.toString()});
        }
        return faceCount;
    }

    private int fillFromTripleSingle(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int n13, int n14, int n22, int n34){
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST) {
            faceCount = fillFromTripleNeighbor(vertices, faces, faceCount, p1, p2, p3, n13, n14, n22, n34);
            faceCount = fillFromSingle(vertices, faces, faceCount, p4, n13, n14, n34);
        }
        return faceCount;
    }

    //c0, c2, c5, c7, c1, c3, c4, c6
    private int fillFromQuadDisjoint(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int n1, int n2, int n3, int n4){
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST) {
            faceCount = fillFromSingle(vertices, faces, faceCount, p1, n1, n2, n3);
            faceCount = fillFromSingle(vertices, faces, faceCount, p2, n1, n2, n4);
            faceCount = fillFromSingle(vertices, faces, faceCount, p3, n4, n3, n1);
            faceCount = fillFromSingle(vertices, faces, faceCount, p4, n3, n4, n2);
        }
        return faceCount;
    }

    // c2, c3, c7, c4, c5, c1, c0, c6
    // p1, p2, p3, p4, p5, n1, n2, n3
    private int fillFromQuintDoubleSingle(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int n1, int n2, int n3){
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p5, n1);
            Vec3 v2 = getMidpoint(p2, n2);
            Vec3 v3 = getMidpoint(p4, n2);
            Vec3 v4 = getMidpoint(p1, n3);
            Vec3 v5 = getMidpoint(p3, n3);
            Vec3 v6 = getMidpoint(p5, n3);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            vertices.put(v5.toString(), String.format("v %s %s %s\n", v5.getX(), v5.getY(), v5.getZ()));
            vertices.put(v6.toString(), String.format("v %s %s %s\n", v6.getX(), v6.getY(), v6.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v2.toString(), v3.toString()});
            faces.put(faceCount++, new String[]{v4.toString(), v5.toString(), v6.toString()});
        }
        return faceCount;
    }

    //c0, c4, c5, c6, c7, c1, c2, c3
    //p1, p2, p3, p4, p5, n1, n2, n3
    private int fillFromQuintCorner(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int n1, int n2, int n3){
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p1, n3);
            Vec3 v2 = getMidpoint(p3, n1);
            Vec3 v3 = getMidpoint(p5, n3);
            Vec3 v4 = getMidpoint(p4, n2);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v2.toString(), v3.toString()});
            faces.put(faceCount++, new String[]{v2.toString(), v3.toString(), v4.toString()});
        }
        return faceCount;
    }

    //c1, c2, c3, c4, c5, c7, c0, c6
    //p1, p2, p3, p4, p5, p6, n1, n2
    private int fillFromHexOpposite(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int p6, int n1, int n2){
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST && data[p6] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p3, n1);
            Vec3 v2 = getMidpoint(p4, n1);
            Vec3 v3 = getMidpoint(p5, n2);
            Vec3 v4 = getMidpoint(p6, n2);
            Vec3 v5 = getMidpoint(p2, n2);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            vertices.put(v5.toString(), String.format("v %s %s %s\n", v5.getX(), v5.getY(), v5.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v3.toString(), v4.toString(), v5.toString()});
        }
        return faceCount;
    }

    //c1, c2, c3, c6, c7, c4, c0, c5
    //p1, p2, p3, p4, p5, p6, n1, n2
    private int fillFromHexDiagonal(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int p6, int n1, int n2) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST && data[p6] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n1);
            Vec3 v1 = getMidpoint(p3, n1);
            Vec3 v2 = getMidpoint(p6, n1);
            Vec3 v3 = getMidpoint(p6, n2);
            Vec3 v4 = getMidpoint(p4, n2);
            Vec3 v5 = getMidpoint(p1, n2);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            vertices.put(v4.toString(), String.format("v %s %s %s\n", v4.getX(), v4.getY(), v4.getZ()));
            vertices.put(v5.toString(), String.format("v %s %s %s\n", v5.getX(), v5.getY(), v5.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v3.toString(), v4.toString(), v5.toString()});
        }
        return faceCount;
    }

    //c2, c3, c4, c5, c6, c7, c0, c1
    //p1, p2, p3, p4, p5, p6, n1, n2
    private int fillFromHexCorner(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int p6, int n1, int n2) {
        if (data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST && data[p6] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(p1, n2);
            Vec3 v1 = getMidpoint(p4, n2);
            Vec3 v2 = getMidpoint(p2, n1);
            Vec3 v3 = getMidpoint(p3, n1);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            vertices.put(v3.toString(), String.format("v %s %s %s\n", v3.getX(), v3.getY(), v3.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
            faces.put(faceCount++, new String[]{v1.toString(), v2.toString(), v3.toString()});
        }
        return faceCount;
    }

    private int fillFromSeven(HashMap<String, String> vertices, HashMap<Integer, String[]> faces, int faceCount, int p1, int p2, int p3, int p4, int p5, int p6, int p7, int n) {
        if(data[p1] < SURFACE_DIST && data[p2] < SURFACE_DIST && data[p3] < SURFACE_DIST && data[p4] < SURFACE_DIST && data[p5] < SURFACE_DIST && data[p6] < SURFACE_DIST && data[p7] < SURFACE_DIST) {
            Vec3 v0 = getMidpoint(n, p1);
            Vec3 v1 = getMidpoint(n, p3);
            Vec3 v2 = getMidpoint(n, p4);
            vertices.put(v0.toString(), String.format("v %s %s %s\n", v0.getX(), v0.getY(), v0.getZ()));
            vertices.put(v1.toString(), String.format("v %s %s %s\n", v1.getX(), v1.getY(), v1.getZ()));
            vertices.put(v2.toString(), String.format("v %s %s %s\n", v2.getX(), v2.getY(), v2.getZ()));
            faces.put(faceCount++, new String[]{v0.toString(), v1.toString(), v2.toString()});
        }
        return faceCount;
    }


    private Vec3 getCoords(int i) {
        int z = i / (sampleSize * sampleSize) - offset;
        int y = i % (sampleSize * sampleSize) / sampleSize - offset;
        int x = i % sampleSize - offset;
        return new Vec3("", x, y, z);
    }

    private Vec3 getMidpoint(int i, int j) {
        Vec3 v0 = getCoords(i);
        Vec3 v1 = getCoords(j);
        double x = (v0.getX() + v1.getX()) / 2.0;
        double y = (v0.getY() + v1.getY()) / 2;
        double z = (v0.getZ() + v1.getZ()) / 2;
        return new Vec3("", x, y, z);
    }

//    private Vec3 getMidpoint(int i, int j){
//        Vec3 v0 = getCoords(i);
//        Vec3 v1 = getCoords(j);
//        float d0 = data[i];
//        float d1 = data[j];
//        double t = getT(d0, d1);
//        double x = interpolate(v0.getX(), v1.getX(), t);
//        double y = interpolate(v0.getY(), v1.getY(), t);
//        double z = interpolate(v0.getZ(), v1.getZ(), t);
//        return new Vec3("", x, y, z);
//    }

    private double getT(double leftValue, double rightValue) {
        return (0.0 - leftValue) / (rightValue - leftValue);
    }

    public static double interpolate(double a, double b, double t) {
        return (a * (1.0 - t)) + (b * t);
    }
}
