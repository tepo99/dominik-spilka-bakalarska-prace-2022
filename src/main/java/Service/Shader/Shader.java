package Service.Shader;

import Factory.Solid.CubeFactory;
import Factory.Solid.CylinderFactory;
import Factory.Solid.SphereFactory;
import Model.Element.Solid;
import Model.Scene;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Shader {
    private String code;

    //@formatter:off
    private  final String vertexShaderSource =
            """
                    #version 330 core
                    layout (location = 0) in vec3 aPos;

                    void main()
                    {
                        gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
                    }""";
    private String computeCode;


    public Shader(Scene scene) {
        StringBuilder shaderSource = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/shader.frag"));
            String line;
            while((line = reader.readLine()) != null) {
                shaderSource.append(line).append('\n');
            }
            reader.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        String code = shaderSource.toString().replace("/**UNIFORMS**/", scene.getUniforms());
        this.code = code.replace("/**DISTANCE_FUNCTION**/", scene.getFunction());
        String computeCode = "";
        try {
            computeCode = Files.readString(Path.of("src/main/resources/compute.frag"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.computeCode = computeCode.replace("/**UNIFORMS**/", scene.getUniforms()).replace("/**DISTANCE_FUNCTION**/", scene.getFunction());
    }

    public String getFragmentShaderSource(){
        return code;
    }

    public String getComputeShaderSource() {
        return this.computeCode;
    }

    public String getVertexShaderSource() {
        return vertexShaderSource;
    }
}
