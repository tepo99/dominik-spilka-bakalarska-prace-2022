package Service.Renderer;

import EventListener.MouseEventListener;
import Model.Camera;
import Model.Element.Element;
import Model.Scene;
import Service.MatrixParser;
import Service.Shader.Shader;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL30C.glDeleteVertexArrays;

public class SceneRenderer {

    private final double startTime;
    private final Camera camera;
    private final Scene scene;
    private int shaderProgram;
    private final MouseEventListener mouseEventListener;

    private  int vbo;
    private  int ebo;
    private  int vao;

    private  final float[] vertices = {
            -1f, -1f, 0f,
            -1f, 1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f,
    };
    private  final int[] indices = {  // note that we start from 0!
            0, 1, 2,  // first Triangle
            1, 2, 3   // second Triangle
    };

    public SceneRenderer(Scene scene, Camera camera, MouseEventListener mouseEventListener) {
        this.scene = scene;
        this.camera = camera;
        this.mouseEventListener = mouseEventListener;
        this.startTime = new Date().getTime();
        initializeCustomFrame();
    }

    public void destroy() {
        dispose();
    }

    //@formatter:on
    public void initializeCustomFrame() {
        Shader shader = new Shader(scene);
        shaderProgram = glCreateProgram();
        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, shader.getFragmentShaderSource());
        glCompileShader(fragmentShader);
        if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) == GL_FALSE) {
//            System.out.println(shader.getFragmentShaderSource());
            throw new RuntimeException("Error creating fragment shader\n"
                    + glGetShaderInfoLog(fragmentShader, glGetShaderi(fragmentShader, GL_INFO_LOG_LENGTH)));
        }

        // Attach the shader
        glAttachShader(shaderProgram, fragmentShader);

        // Link this program
        glLinkProgram(shaderProgram);

        // Check for linking errors
        if (glGetProgrami(shaderProgram, GL_LINK_STATUS) == GL_FALSE)
            throw new RuntimeException("Unable to link shader program:");

        // Generate and bind a Vertex Array
        vao = glGenVertexArrays();
        glBindVertexArray(vao);


        // Create a FloatBuffer of vertices
        FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(vertices.length);
        verticesBuffer.put(vertices).flip();

        // Create a Buffer Object and upload the vertices buffer
        vbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_STATIC_DRAW);

        // Point the buffer at location 0, the location we set
        // inside the vertex shader. You can use any location
        // but the locations should match
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glBindVertexArray(0);
    }

    public void renderCustomFrame() {
        // Use our program
        glUseProgram(shaderProgram);

        int iTime = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "iTime");
        int solidCount = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "solidCount");
        int iMouseX = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "iMouseX");
        int iMouseY = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "iMouseY");
        int position = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "position");
        int look = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "look");
        int up = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, "up");

        ARBShaderObjects.glUniform1fARB(iTime, (float)((new Date().getTime() - startTime) / 1000.0));
        ARBShaderObjects.glUniform1fARB(solidCount, (float)scene.getElementCount());
        ARBShaderObjects.glUniform1fARB(iMouseX, mouseEventListener.getMouseX());
        ARBShaderObjects.glUniform1fARB(iMouseY, mouseEventListener.getMouseY());
        ARBShaderObjects.glUniform3fvARB(look, camera.getLookVector());
        ARBShaderObjects.glUniform3fvARB(up, camera.getUpVector());
        ARBShaderObjects.glUniform3fvARB(position, camera.getPositionVector());
        ARBShaderObjects.glUseProgramObjectARB(shaderProgram);

//        for(Parameter parameter: scene.getParameters()) {
//            int pointer = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, parameter.getShaderName());
//            switch (parameter.getType()) {
//                case "float" -> ARBShaderObjects.glUniform1fARB(pointer, (float) parameter.getValue());
//                case "vec3" -> ARBShaderObjects.glUniform3fvARB(pointer, (float[]) parameter.getValue());
//                case "vec4" -> ARBShaderObjects.glUniform4fvARB(pointer, (float[]) parameter.getValue());
//            }
//        }

        List<Element> elements = new ArrayList<>();
        scene.getElements(scene.getRoot(), elements);
        for(Element e: elements) {
            int pointer = ARBShaderObjects.glGetUniformLocationARB(shaderProgram, e.getTransformShaderName());
            ARBShaderObjects.glUniformMatrix4fvARB(pointer, false, MatrixParser.matToArray(e.getTransform()));
        }

        // Bind the vertex array and enable our location
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);

        // Draw a triangle of 3 vertices
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        // Disable our location
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);

        if(camera.isControlled()) {
            mouseEventListener.look();
            mouseEventListener.move();
        }
        if(scene.isMoveX() || scene.isMoveY() || scene.isMoveZ()) {
            mouseEventListener.moveObject();
        }

        // Un-bind our program
        glUseProgram(0);
    }

    private void dispose() {

        // Dispose the vertex array
        glBindVertexArray(0);
        glDeleteVertexArrays(vao);

        // Dispose the buffer object
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(vbo);
    }

}
