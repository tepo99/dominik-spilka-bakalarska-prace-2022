package Service.Renderer;

import EventListener.MouseEventListener;
import Factory.Solid.VoidFactory;
import Model.Camera;
import Model.Mode;
import Model.Scene;
import View.Components.*;
import org.liquidengine.legui.DefaultInitializer;
import org.liquidengine.legui.component.*;
import org.liquidengine.legui.style.color.ColorConstants;
import org.liquidengine.legui.system.context.CallbackKeeper;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.glfw.GLFWWindowCloseCallbackI;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class UserInterfaceRenderer {
    private static final int WIDTH = 1920, HEIGHT = 1080;
    private final MouseEventListener mouseEventListener;
    private final Camera camera;
    private final Scene scene;
    private final DefaultInitializer initializer;
    private volatile boolean running = true, hiding = false;
    private Widget solidsListWidget;
    private final SolidsList solidsList;
    private Frame frame;
    private Widget transformWidget;
    private TransformControls transformControls = new TransformControls(new VoidFactory().build(null));
    private Widget addWidget;

    public UserInterfaceRenderer(Scene scene, Camera camera, long window, MouseEventListener mouseEventListener) {
        this.scene = scene;
        this.camera = camera;
        this.solidsList = new SolidsList(scene);
        this.mouseEventListener = mouseEventListener;
        frame = this.createFrameWithGUI();
        initializer = new DefaultInitializer(window, frame);
        initializeGuiWithCallbacks();
    }

    public Frame createFrameWithGUI() {
        frame = new Frame(WIDTH, HEIGHT);
        // Set background color for frame
        frame.getContainer().getStyle().getBackground().setColor(ColorConstants.transparent());
        frame.getContainer().setFocusable(false);

        Widget controls = new Controls(scene);

        solidsListWidget = new Widget();
        solidsListWidget.setSize(200, 300);
        solidsListWidget.setPosition(0, HEIGHT - 300);

        transformWidget = new Widget();
        transformWidget.setSize(200, 300);
        transformWidget.setPosition(WIDTH - 200, HEIGHT - 300);

        generateOnFly();

        frame.getContainer().add(controls);
        frame.getContainer().add(solidsListWidget);
        frame.getContainer().add(transformWidget);

        return frame;
    }

    public DefaultInitializer getInitializer() {
        return initializer;
    }

    public Frame getFrame() {
        return frame;
    }

    private void initializeGuiWithCallbacks() {
        GLFWKeyCallbackI escapeCallback = (w1, key, code, action, mods) -> running = !(key == GLFW_KEY_ESCAPE && action != GLFW_RELEASE);

        // used to skip gui rendering
        GLFWKeyCallbackI hideCallback = (w1, key, code, action, mods) -> {
            if (key == GLFW_KEY_H && action == GLFW_RELEASE)
                //noinspection NonAtomicOperationOnVolatileField
                hiding = !hiding;
            else if (key == GLFW_KEY_LEFT_SHIFT && action ==GLFW_PRESS) {
                camera.setControlled(true);
            }
            else if (key == GLFW_KEY_LEFT_SHIFT && action ==GLFW_RELEASE) {
                camera.setControlled(false);
            }
            else if (key == GLFW_KEY_X && action ==GLFW_PRESS) {
                scene.setMoveX(true);
            }
            else if (key == GLFW_KEY_X && action ==GLFW_RELEASE) {
                scene.setMoveX(false);
            }
            else if (key == GLFW_KEY_Y && action ==GLFW_PRESS) {
                scene.setMoveY(true);
            }
            else if (key == GLFW_KEY_Y && action ==GLFW_RELEASE) {
                scene.setMoveY(false);
            }
            else if (key == GLFW_KEY_Z && action ==GLFW_PRESS) {
                scene.setMoveZ(true);
            }
            else if (key == GLFW_KEY_Z && action ==GLFW_RELEASE) {
                scene.setMoveZ(false);
            }
        };
        GLFWWindowCloseCallbackI windowCloseCallback = w -> running = false;

        CallbackKeeper keeper = initializer.getCallbackKeeper();
        keeper.getChainKeyCallback().add(escapeCallback);
        keeper.getChainKeyCallback().add(hideCallback);
        keeper.getChainWindowCloseCallback().add(windowCloseCallback);

        org.liquidengine.legui.system.renderer.Renderer renderer = initializer.getRenderer();
        renderer.initialize();
    }

    public void generateOnFly() {
        solidsListWidget.getContainer().clearChildComponents();
        for (ListElement c : solidsList.getListElements()) {
            solidsListWidget.getContainer().add(c);
        }
        transformWidget.getContainer().clearChildComponents();
        mouseEventListener.unsubscribe(transformControls);
        if(scene.getSelected() != null) {
            transformControls = new TransformControls(scene.getSelected());
            transformWidget.getContainer().add(transformControls);
            mouseEventListener.subscribe(transformControls);
        }
        if(scene.getMode() == Mode.ADD && !frame.getContainer().contains(addWidget)) {
            addWidget = new AddWidget(scene);
            frame.getContainer().add(addWidget);
        } else {
            frame.getContainer().remove(addWidget);
        }
    }

    public boolean isRunning() {
        return running;
    }

    public boolean isHiding() {
        return hiding;
    }

    public SolidsList getSolidsList() {
        return solidsList;
    }
}
