package EventListener.Subscribers;

import EventListener.MouseEventListener;
import Model.Subscribers.SceneSubscriber;

import java.util.ArrayList;
import java.util.List;

public interface MouseEventPublisher {
    List<MouseEventSubscriber> MOUSE_EVENT_SUBSCRIBER = new ArrayList<>();

    default void subscribe(MouseEventSubscriber sceneSubscriber) {
        MOUSE_EVENT_SUBSCRIBER.add(sceneSubscriber);
    }

    default void unsubscribe(MouseEventSubscriber sceneSubscriber) {
        MOUSE_EVENT_SUBSCRIBER.remove(sceneSubscriber);
    }

    default void update() {
        for(MouseEventSubscriber s: MOUSE_EVENT_SUBSCRIBER) {
            s.react();
        }
    }
}
