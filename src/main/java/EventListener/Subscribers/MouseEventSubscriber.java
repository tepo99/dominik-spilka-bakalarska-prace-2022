package EventListener.Subscribers;

public interface MouseEventSubscriber {
    void react();
}
