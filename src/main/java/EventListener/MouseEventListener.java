package EventListener;

import EventListener.Subscribers.MouseEventPublisher;
import Model.Element.Element;
import Model.Scene;
import Model.Camera;
import org.lwjgl.BufferUtils;

import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class MouseEventListener implements MouseEventPublisher {

    private float mouseOriginX, mouseOriginY, mouseX, mouseY;
    private boolean leftClicked = false;
    private final long window;

    private final Scene scene;
    private final Camera camera;
    private boolean rightClicked = false;

    public MouseEventListener(Camera camera, long window, Scene scene) {
        this.camera = camera;
        this.window = window;
        this.scene = scene;
    }

    public float getMouseX() {
        return mouseX;
    }

    public float getMouseY() {
        return mouseY;
    }

    public void look() {
        int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        if (state == GLFW_PRESS) {
            DoubleBuffer rawMouseX = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer rawMouseY = BufferUtils.createDoubleBuffer(1);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwGetCursorPos(window, rawMouseX, rawMouseY);
            rawMouseX.rewind();
            rawMouseY.rewind();
            //mousePressed
            if (!leftClicked) {
                this.mouseOriginX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
                this.mouseOriginY = -(float) ((rawMouseY.get(0) - 1080) / 1080.0);
                leftClicked = true;
            }
            Camera originalCamera = camera;
            this.mouseX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
            this.mouseY = -(float) ((rawMouseY.get(0) - 1080) / 1080.0);
            float dX = mouseX - mouseOriginX;
            float dY = mouseY - mouseOriginY;
            double azimuth = originalCamera.getAzimuth();
            double zenith = originalCamera.getZenith();
            azimuth += dX;
            camera.setAzimuth(azimuth);
            zenith += dY;
            camera.setZenith(zenith);

            this.mouseOriginX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
            this.mouseOriginY = -(float) ((rawMouseY.get(0) - 1080) / 1080.0);
            //mouseReleased
        }
        if (state == GLFW_RELEASE) {
            if (leftClicked) {
                leftClicked = false;
            }
        }
    }

    public void move(){
        int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
        if (state == GLFW_PRESS) {
            DoubleBuffer rawMouseX = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer rawMouseY = BufferUtils.createDoubleBuffer(1);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwGetCursorPos(window, rawMouseX, rawMouseY);
            rawMouseX.rewind();
            rawMouseY.rewind();
            //mousePressed
            if (!rightClicked) {
                this.mouseOriginX = (float) ((rawMouseX.get(0) - 400) / 400.0);
                this.mouseOriginY = -(float) ((rawMouseY.get(0) - 300) / 300.0);
                rightClicked = true;
            }
            this.mouseX = (float) ((rawMouseX.get(0) - 400) / 400.0);
            this.mouseY = -(float) ((rawMouseY.get(0) - 300) / 300.0);
            float[] positionVector = camera.getPositionVector();
            float[] originalPositionVector = new float[]{positionVector[0], positionVector[1], positionVector[2]};
            float dX = mouseX - mouseOriginX;
            float dY = mouseY - mouseOriginY;
            double azimuth = camera.getAzimuth();
            float[] upVector = camera.getUpVector();
            positionVector[0] = (originalPositionVector[0] + (float)Math.cos(azimuth - Math.PI/2) * dX);
            positionVector[2] = (originalPositionVector[2] + (float)Math.sin(azimuth - Math.PI/2) * dX);
            positionVector[0] = positionVector[0] - dY * upVector[0];
            positionVector[1] = originalPositionVector[1] - dY * upVector[1];
            positionVector[2] = positionVector[2] - dY * upVector[2];
            this.mouseOriginX = (float) ((rawMouseX.get(0) - 400) / 400.0);
            this.mouseOriginY = -(float) ((rawMouseY.get(0) - 300) / 300.0);
            camera.setPositionVector(positionVector);
            //mouseReleased
        }
        if (state == GLFW_RELEASE) {
            if (rightClicked) {
                rightClicked = false;
            }
        }
    }

    public void moveObject() {
        int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        if (state == GLFW_PRESS && scene.getSelected() != null) {
            DoubleBuffer rawMouseX = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer rawMouseY = BufferUtils.createDoubleBuffer(1);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwGetCursorPos(window, rawMouseX, rawMouseY);
            rawMouseX.rewind();
            rawMouseY.rewind();
            //mousePressed
            if (!leftClicked) {
                this.mouseOriginX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
                leftClicked = true;
            }
            this.mouseX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
            float dx = mouseX - mouseOriginX;
            Element element = scene.getSelected();
            switch (scene.getMode()) {
                case MOVE -> {
                    translate(element, dx);
                    return;
                }
                case ROTATE -> {
                    rotate(element, dx);
                    return;
                }
                case SCALE -> {
                    scale(element, dx);
                    return;
                }
            }
            this.mouseOriginX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
            update();

            this.mouseOriginX = (float) ((rawMouseX.get(0) - 1920) / 1920.0);
            //mouseReleased
        }
        if (state == GLFW_RELEASE) {
            if (leftClicked) {
                leftClicked = false;
            }
        }
    }

    private void translate(Element element, float dx) {
        if (scene.isMoveX()) {
            element.setTranslationx(element.getTranslationx() + dx);
        } else if (scene.isMoveY()) {
            element.setTranslationy(element.getTranslationy() + dx);
        } else if (scene.isMoveZ()) {
            element.setTranslationz(element.getTranslationz() + dx);
        }
    }

    private void rotate(Element element, float dx) {
        if (scene.isMoveX()) {
            element.setRotx(element.getRotx() + dx);
        } else if (scene.isMoveY()) {
            element.setRoty(element.getRoty() + dx);
        } else if (scene.isMoveZ()) {
            element.setRotz(element.getRotz() + dx);
        }
    }

    private void scale(Element element, float dx) {
        if (scene.isMoveX()) {
            element.setScalex(element.getScalex() * 1 + (dx/10));
        } else if (scene.isMoveY()) {
            element.setScaley(element.getScaley() * 1 + (dx/10));
        } else if (scene.isMoveZ()) {
            element.setScalez(element.getScalez() * 1 + (dx/10));
        }
    }


}
