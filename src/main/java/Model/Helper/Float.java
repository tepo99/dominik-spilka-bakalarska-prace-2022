package Model.Helper;

import org.joml.Matrix4f;

public class Float implements Parameter<java.lang.Float>{

    private static final String type = "float";

    private String name;
    private double val;

    public Float(String name, double val) {
        this.name = name;
        this.val = val;
    }

    public String getName() {
        return name;
    }

    @Override
    public java.lang.Float getValue() {
        return (float) val;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getVal() {
        return val;
    }

    public void setVal(double val) {
        this.val = val;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String toShader() {
        return String.format("%f", val);
    }

    @Override
    public String getShaderName() {
        return this.name + this.hashCode();
    }

    @Override
    public Float mul(Matrix4f mat) {
        return new Float(name, val);
    }
}
