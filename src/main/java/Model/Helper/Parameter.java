package Model.Helper;

import org.joml.Matrix4f;

public interface Parameter<T> {

    String getType();

    String toShader();

    String getShaderName();

    String getName();

     T getValue();

     Parameter mul(Matrix4f mat);
}
