package Model.Helper;

import org.joml.Matrix4f;
import org.joml.Vector4f;

import java.util.Objects;

public class Vec3 implements Parameter<float[]>{

    private static final String type = "vec3";

    private String name;
    private double x;
    private double y;
    private double z;

    public Vec3(String name, double x, double y, double z) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getName() {
        return name;
    }

    @Override
    public float[] getValue() {
        return new float[]{
                (float)x,
                (float)y,
                (float)z
        };
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getShaderName() {
        return this.name + this.hashCode();
    }

    @Override
    public String toShader() {
        return String.format("vec3(%f, %f, %f)", x, y, z);
    }

    @Override
    public Vec3 mul(Matrix4f mat) {
        Vector4f vec = new Vector4f((float) x, (float) y, (float) z, 1.0f);
        vec.mul(mat);
        return new Vec3(name, vec.x, vec.y, vec.z);
    }

    public boolean equals(Vec3 o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        double e = 0.000001;
        return Math.abs(o.x - x) < e && Math.abs(o.y - y) < e && Math.abs(o.z - z) < e;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(toString());
    }

    @Override
    public String toString() {
        return "Vec3{" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
