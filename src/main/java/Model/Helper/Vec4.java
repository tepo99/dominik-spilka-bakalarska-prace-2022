package Model.Helper;

import org.joml.Matrix4f;
import org.joml.Vector4f;

public class Vec4 implements Parameter<float[]> {

    private static final String type = "vec4";

    private String name;
    private double x;
    private double y;
    private double z;
    private double w;

    public Vec4(String name, double x, double y, double z, double w) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public String getName() {
        return name;
    }

    @Override
    public float[] getValue() {
        return new float[]{
                (float)x,
                (float)y,
                (float)z,
                (float)w
        };
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getShaderName() {
        return this.name + this.hashCode();
    }

    public String toShader() {
        return String.format("vec4(%f, %f, %f, %f)", x, y, z, w);
    }

    @Override
    public Vec4 mul(Matrix4f mat) {
        Vector4f vec = new Vector4f((float) x, (float) y, (float) z, (float) w);
        vec.mul(mat);
        return new Vec4(name, vec.x, vec.y, vec.z, vec.w);
    }
}
