package Model.Helper;

public class Point3D {
    private final double x;
    private final double y;
    private final double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(Point3D point) {
        this.x = point.getX();
        this.y = point.getY();
        this.z = point.getZ();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Point3D withX(double x) {
        return new Point3D(x, y, z);
    }

    public Point3D withY(double y) {
        return new Point3D(x, y, z);
    }

    public Point3D withZ(double z) {
        return new Point3D(x, y, z);
    }
}
