package Model.Subscribers;

public interface SceneSubscriber {
    void react();
}
