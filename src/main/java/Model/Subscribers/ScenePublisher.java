package Model.Subscribers;

import java.util.ArrayList;
import java.util.List;

public interface ScenePublisher {
    List<SceneSubscriber> SCENE_SUBSCRIBERS = new ArrayList<>();

    default void subscribe(SceneSubscriber sceneSubscriber) {
        SCENE_SUBSCRIBERS.add(sceneSubscriber);
    }

    default void unsubscribe(SceneSubscriber sceneSubscriber) {
        SCENE_SUBSCRIBERS.remove(sceneSubscriber);
    }

    default void update() {
        for(SceneSubscriber s: SCENE_SUBSCRIBERS) {
            s.react();
        }
    }
}
