package Model.Subscribers;

import java.util.ArrayList;
import java.util.List;

public interface SolidsListPublisher {
    List<SolidsListSubscriber> SOLIDS_LIST_SUBSCRIBERS = new ArrayList<>();

    default void subscribe(SolidsListSubscriber subscriber) {
        SOLIDS_LIST_SUBSCRIBERS.add(subscriber);
    }

    default void unsubscribe(SolidsListSubscriber subscriber) {
        SOLIDS_LIST_SUBSCRIBERS.remove(subscriber);
    }

    default void update() {
        for(SolidsListSubscriber s: SOLIDS_LIST_SUBSCRIBERS) {
            s.react();
        }
    }
}
