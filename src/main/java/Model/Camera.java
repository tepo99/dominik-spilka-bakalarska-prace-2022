package Model;

public class Camera {

    private boolean renderPaused = false;
    private float[] positionVector = new float[]{-5, 0, 0};
    private double azimuth = 0;
    private double zenith = 0;

    private boolean controlled = false;

    public float[] getLookVector() {
        float[] lookVector = new float[3];
        lookVector[0] = (float) Math.cos(azimuth) * (float) Math.cos(zenith);
        lookVector[1] = (float) Math.sin(zenith);
        lookVector[2] = (float) Math.sin(azimuth) * (float) Math.cos(zenith);
        return lookVector;
    }

    public float[] getUpVector() {
        float[] lookVector = new float[3];
        lookVector[0] = (float) Math.cos(azimuth) * (float) Math.cos(zenith + Math.PI / 2.0);
        lookVector[1] = (float) Math.sin(zenith + Math.PI / 2.0);
        lookVector[2] = (float) Math.sin(azimuth) * (float) Math.cos(zenith + Math.PI / 2.0);
        return lookVector;
    }

    public boolean isRenderPaused() {
        return renderPaused;
    }

    public void setRenderPaused(boolean renderPaused) {
        this.renderPaused = renderPaused;
    }

    public float[] getPositionVector() {
        return positionVector;
    }

    public void setPositionVector(float[] positionVector) {
        this.positionVector = positionVector;
    }

    public double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    public double getZenith() {
        return zenith;
    }

    public void setZenith(double zenith) {
        if(zenith > Math.PI / 2) {
            this.zenith = Math.PI / 2;
            return;
        }
        if(zenith < -Math.PI / 2) {
            this.zenith = - Math.PI / 2;
            return;
        }
        this.zenith = zenith;
    }

    public boolean isControlled() {
        return controlled;
    }

    public void setControlled(boolean controlled) {
        this.controlled = controlled;
    }
}
