package Model;

import Factory.Abstract.SolidFactory;
import Factory.Operator.*;
import Factory.Solid.*;
import Model.Element.Element;
import Model.Element.Modifier;
import Model.Element.Operator;
import Model.Element.Solid;
import Model.Helper.Parameter;
import Model.Subscribers.ScenePublisher;
import Service.Computer;
import Service.Saving.XmlLoad;
import View.Components.ListElement;
import org.apache.commons.text.StringSubstitutor;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

public class Scene implements ScenePublisher {
    private Element root;
    private Element selected = null;
    private Mode mode = Mode.MOVE;

    private boolean moveX, moveY, moveZ;
    private int elementCount = 0;

    public Scene() {
        Operator add = new AdditionFactory().build(null);
        Solid cube = new CubeFactory().build(add);
        Solid sphere = new CubeFactory().build(add);
        sphere.setTranslationx(3);
        sphere.setTranslationy(3);
        cube.setRotx((float) Math.toRadians(45));
        cube.setRoty((float) Math.toRadians(45));
        add.setLeft(sphere);
        add.setRight(cube);
        add.setTranslationx(-.2f);
        root = add;
        elementCount = 3;
    }

    public void setRoot(Element root) {
        deconstructTree(this.root);
        this.root = root;
        update();
    }

    private void deconstructTree(Element e) {
        if(e.getChildren().size() > 0) {
            for(Element child: e.getChildren()) {
                deconstructTree(child);
            }
        }
        e.setChildren(new ArrayList<>());
        e.setParent(null);
    }

    public String getFunction(){
        Map<String, String> map = new HashMap<>();
        map.put("point", "point");
        return StringSubstitutor.replace(root.getFunction(), map);
    }

    public String getUniforms() {
        StringBuilder builder = new StringBuilder();
        List<Element> elements = new ArrayList<>();
        getElements(root, elements);
        for(Element e: elements) {
            builder.append(e.getTransformShaderInit());
        }
        return builder.toString();
    }

    public void getElements(Element element, List<Element> list) {
        list.add(element);
        if(null == element.getChildren() || element.getChildren().size() <= 0) {
            return;
        }
        for(Element e : element.getChildren()) {
            getElements(e, list);
        }
    }

    public Element getRoot() {
        return root;
    }

    public List<Parameter> getParameters() {
        return root.getParameters();
    }

    public void setSelected(Element element) {
        selected = element;
    }

    public Element getSelected() {
        return selected;
    }

    public boolean isMoveX() {
        return moveX;
    }

    public void setMoveX(boolean moveX) {
        this.moveX = moveX;
    }

    public boolean isMoveY() {
        return moveY;
    }

    public void setMoveY(boolean moveY) {
        this.moveY = moveY;
    }

    public boolean isMoveZ() {
        return moveZ;
    }

    public void setMoveZ(boolean moveZ) {
        this.moveZ = moveZ;
    }


    public void addSolid(Solid solid, Element element) {
        if(element.equals(root)){
            root = solid;
            update();
            return;
        }
        if(element.getName().equals("Void")) {
            for(Element e: element.getParent().getChildren()) {
                if(e.equals(element)) {
                    e.getParent().getChildren().set(e.getParent().getChildren().indexOf(e), solid);
                }
            }
        }
        Operator addition = new AdditionFactory().build(element.getParent());
        element.setParent(addition);
        solid.setParent(addition);
        addition.setLeft(element);
        addition.setRight(solid);
        if(null == addition.getParent()) {
            root = addition;
            update();
            return;
        }
        Element parent = addition.getParent();
        for(Element e: parent.getChildren()) {
            if(e.equals(element)) {
                parent.getChildren().set(parent.getChildren().indexOf(e), addition);
            }
        }
        elementCount+=2;
        update();
    }

    public void addOperator(Operator operator, Element element) {
        if(element instanceof Operator) {
            if(null == element.getParent()){
                root = operator;
            } else {
                operator.setParent(element.getParent());
                element.getParent().getChildren().forEach((item) -> {
                    if (item.equals(element)) {
                        item.getParent().getChildren().set(item.getParent().getChildren().indexOf(item), operator);
                    }});
            }
            operator.setLeft(((Operator) element).getLeft());
            operator.setRight(((Operator) element).getRight());
            update();
            return;
        }
        operator.setParent(element.getParent());
        operator.setLeft(element);
        operator.setRight(new VoidFactory().build(operator));
        element.setParent(operator);
        if(element.equals(root)){
            root = operator;
            update();
            return;
        }
        for(Element e: operator.getParent().getChildren()) {
            if(e.equals(element)) {
                operator.getParent().getChildren().set(operator.getParent().getChildren().indexOf(e), operator);
            }
        }
        elementCount++;
        update();
    }

    public void addModifier(Modifier modifier, Element element) {
        if(selected instanceof Modifier) {
            if(null == selected.getParent()){
                root = modifier;
            } else {
                modifier.setParent(selected.getParent());
                selected.getParent().getChildren().forEach((item) -> {
                    if (item.equals(element)) {
                        selected.getParent().getChildren().set(selected.getParent().getChildren().indexOf(item), modifier);
                    }
                });
            }
            modifier.setChildren(selected.getChildren());
            update();
            return;
        }
        modifier.setParent(element.getParent());
        modifier.setLeft(element);
        if(modifier.getParent() != null) {
            for (Element e : modifier.getParent().getChildren()) {
                if (e.equals(element)) {
                    modifier.getParent().getChildren().set(modifier.getParent().getChildren().indexOf(e), modifier);
                }
            }
        }
        update();
    }

    public void updateFromList(List<ListElement> list) {
        for (ListElement e: list) {
            e.setIllegal(true);
        }
        int indent = 0;
        ListElement rootListElement = list.get(0);
        this.root = buildLayer(indent, rootListElement, list);
    }

    private Element buildLayer(int indent, ListElement listElement, List<ListElement> list){
        listElement.setIllegal(false);
        if(listElement.getElement() instanceof Solid) {
            return listElement.getElement();
        }
        indent++;
        int finalIndent = indent;
        List<ListElement> currentLayer = list.stream().filter(element -> element.getIndent() == finalIndent && element.getOrder() > listElement.getOrder()).toList();
        if(currentLayer.size() < 1) {
            SolidFactory voidFactory = new VoidFactory();
            if(listElement.getElement() instanceof Modifier){
                ((Modifier) listElement.getElement()).setLeft(voidFactory.build(listElement.getElement()));
            }
            if(listElement.getElement() instanceof Operator) {
                ((Operator) listElement.getElement()).setLeft(voidFactory.build(listElement.getElement()));
                ((Operator) listElement.getElement()).setRight(voidFactory.build(listElement.getElement()));
            }
            listElement.setIllegal(true);
            return listElement.getElement();
        }
        ListElement left = currentLayer.get(0);
        ListElement right;
        Element leftChild = buildLayer(indent, left, list);
        if(listElement.getElement() instanceof Operator && currentLayer.size() >= 2) {
            right = currentLayer.get(1);
            Element rightChild = buildLayer(indent, right, list);
            ((Operator) listElement.getElement()).setLeft(leftChild);
            ((Operator) listElement.getElement()).setRight(rightChild);
            leftChild.setParent(listElement.getElement());
            rightChild.setParent(listElement.getElement());
        } else if (listElement.getElement() instanceof Modifier) {
            ((Modifier) listElement.getElement()).setLeft(leftChild);
            leftChild.setParent(listElement.getElement());
        }
        return listElement.getElement();
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public int getElementCount() {
        return elementCount;
    }

    public void setElementCount(int elementCount) {
        this.elementCount = elementCount;
    }

    public void deleteElement(Element element, Element current) {
        if(element.equals(root)){
            root = new VoidFactory().build(null);
            update();
            return;
        }
        if(current.equals(element)){
            List<Element> children = element.getParent().getChildren();
            for(Element e: children) {
                if(e.equals(element)) {
                    children.set(children.indexOf(e), new VoidFactory().build(e.getParent()));
                }
            }
            element.getParent().setChildren(children);
            update();
            return;
        }
        for(Element e: current.getChildren()) {
            deleteElement(element, e);
        }
    }
}
