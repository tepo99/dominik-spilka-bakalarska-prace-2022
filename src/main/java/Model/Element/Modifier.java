package Model.Element;

import Model.Helper.Parameter;
import org.apache.commons.text.StringSubstitutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Modifier extends Element {


    public Modifier(String name, String function, Element parent) {
        this.name = name;
        this.function = function;
        this.parent = parent;
    }

    public void setLeft(Element element) {
        if (children.size() < 1) {
            children.add(element);
            return;
        }
        children.set(0, element);
    }

    @Override
    public String getFunction() {
//        String point = "transform($${point}, mat4(" + getTransformShaderName() + "))";
//        System.out.println(function);
//        String thisFunc = String.format(function, getLeft());
        String point = function.replace("$${point}", "transform($${point}, mat4(" + getTransformShaderName() + "))");
        System.out.println(point);
        String function = getLeft().getFunction();
        System.out.println(function);
        HashMap<String, String> map = new HashMap<>();
        map.put("point", point);
        function = StringSubstitutor.replace(function, map);
        map = new HashMap<>();
        map.put("func", getLeft().getFunction());
        System.out.println(function);
        return StringSubstitutor.replace(function, map);
    }

    @Override
    public List<Parameter> getParameters() {
        List<Parameter> parameters = new ArrayList<>();
        parameters.addAll(this.parameters);
        parameters.addAll(children.get(0).getParameters());
        return parameters;
    }

    public Element getLeft() {
        return this.children.get(0);
    }
}
