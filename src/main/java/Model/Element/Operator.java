package Model.Element;

import Model.Helper.Parameter;
import Service.MatrixParser;
import org.apache.commons.text.StringSubstitutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Operator extends Element{

    public Operator(String name, String function, Element parent) {
        this.name = name;
        this.function = function;
        this.parent = parent;
    }

    public void setLeft(Element left) {
        if(children.size() < 1) {
            children.add(left);
            return;
        }
        children.set(0, left);
    }

    public void setRight(Element right) {
        if(children.size() < 2) {
            children.add(right);
            return;
        }
        children.set(1, right);
    }

    @Override
    public String getFunction() {
        String point = "transform($${point}, mat4(" + getTransformShaderName() + "))";
        String function = String.format(this.function, getLeft().getFunction(), getRight().getFunction());
        HashMap<String, String> map = new HashMap<>();
        map.put("point", point);
        return StringSubstitutor.replace(function, map);

    }

    @Override
    public List<Parameter> getParameters() {
        List<Parameter> parameters = new ArrayList<>();
        parameters.addAll(this.parameters);
        parameters.addAll(getLeft().getParameters());
        parameters.addAll(getRight().getParameters());
        return parameters;
    }


    @Override
    public void setChildren(List<Element> children) {
        this.children = children;
    }

    public Element getLeft() {
        return children.get(0);
    }

    public Element getRight() {
        return children.get(1);
    }
}
