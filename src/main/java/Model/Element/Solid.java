package Model.Element;

import Model.Helper.Parameter;
import java.util.List;

public class Solid extends Element{

    public Solid(String name, String function, List<Parameter> parameters, Element parent) {
        this.name = name;
        this.function = function;
        this.parameters = parameters;
        this.parent = parent;
    }
}
