package Model.Element;

import Model.Helper.Parameter;
import Service.MatrixParser;
import org.apache.commons.text.StringSubstitutor;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public abstract class Element {
    protected String function;
    protected String name;
    protected List<Parameter> parameters = new ArrayList<>();
    protected List<Element> children = new ArrayList<>();
    protected Element parent = null;
    protected float translationx = 0, translationy = 0, translationz = 0;
    protected float rotx = 0, roty = 0, rotz = 0;
    protected float scalex = 1, scaley = 1, scalez = 1;
    private boolean souted = false;

    public String getName() {
        return name;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public String getFunctionPure() {
        return this.function;
    }

    public String getFunction() {
        String point = String.format("transform($${point}, %s)", getTransformShaderName());
        HashMap<String, String> map = new HashMap<>();
        map.put("point", point);
        return StringSubstitutor.replace(function, map);
    }

    public Matrix4f getTransform(){
        Matrix4f mat = new Matrix4f().identity();
        mat.mul(MatrixParser.getTranslation(translationx, translationy, translationz));
        mat.mul(MatrixParser.getRot(rotx, roty, rotz));
        mat.mul(MatrixParser.getScale(scalex, scaley, scalez));
//        if(!souted) {
//            System.out.print(getTransformShaderName() + " ");
//            System.out.println(MatrixParser.parse(mat));
//            souted = true;
//        }
        mat.invert();
        return mat;
    }

    public String getTransformShaderInit() {
        return String.format("uniform mat4 %s;\n", getTransformShaderName());
    }

    public String getTransformShaderName() {
        return getName() + hashCode() + "transform";
    }

    public List<Element> getChildren() {
        return children;
    }

    public Element getParent() {
        return parent;
    }

    public void setParent(Element parent) {
        this.parent = parent;
    }

    public void setChildren(List<Element> children){
        this.children = children;
    }

    public float getTranslationx() {
        return translationx;
    }

    public void setTranslationx(float translationx) {
        this.translationx = translationx;
    }

    public float getTranslationy() {
        return translationy;
    }

    public void setTranslationy(float translationy) {
        this.translationy = translationy;
    }

    public float getTranslationz() {
        return translationz;
    }

    public void setTranslationz(float translationz) {
        this.translationz = translationz;
    }

    public float getRotx() {
        return rotx;
    }

    public void setRotx(float rotx) {
        this.rotx = rotx;
    }

    public float getRoty() {
        return roty;
    }

    public void setRoty(float roty) {
        this.roty = roty;
    }

    public float getRotz() {
        return rotz;
    }

    public void setRotz(float rotz) {
        this.rotz = rotz;
    }

    public float getScalex() {
        return scalex;
    }

    public void setScalex(float scalex) {
        this.scalex = scalex;
    }

    public float getScaley() {
        return scaley;
    }

    public void setScaley(float scaley) {
        this.scaley = scaley;
    }

    public float getScalez() {
        return scalez;
    }

    public void setScalez(float scalez) {
        this.scalez = scalez;
    }
}
