package Factory.Abstract;

import Model.Element.Element;
import Model.Element.Operator;
import Model.Element.Solid;
import org.joml.Matrix4f;

public abstract class SolidFactory extends ElementFactory{

    public abstract Solid build(Element parent);

    public static Element buildFromInfo(String name, String function){
        return new Solid(name, function, null, null);
    }
}
