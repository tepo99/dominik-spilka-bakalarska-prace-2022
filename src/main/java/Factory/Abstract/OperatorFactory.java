package Factory.Abstract;

import Factory.Modifier.RoundFactory;
import Model.Element.Element;
import Model.Element.Operator;
import org.joml.Matrix4f;

public abstract class OperatorFactory extends RoundFactory {
    public abstract Operator build(Element parent);
    public static Element buildFromInfo(String name, String function){
        return new Operator(name, function, null);
    }
}
