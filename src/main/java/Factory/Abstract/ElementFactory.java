package Factory.Abstract;

import Model.Element.Element;
import org.joml.Matrix4f;

public abstract class ElementFactory {
    public abstract Element build(Element parent);
}
