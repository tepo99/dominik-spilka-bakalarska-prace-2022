package Factory.Abstract;

import Model.Element.Element;
import Model.Element.Modifier;
import Model.Element.Operator;
import org.joml.Matrix4f;

public abstract class ModifierFactory extends ElementFactory{
    public abstract Modifier build(Element parent);

    public static Element buildFromInfo(String name, String function){
        return new Modifier(name, function, null);
    }
}
