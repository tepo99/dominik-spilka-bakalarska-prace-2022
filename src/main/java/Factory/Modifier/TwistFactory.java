package Factory.Modifier;

import Factory.Abstract.ModifierFactory;
import Model.Element.Element;
import Model.Element.Modifier;

public class TwistFactory extends ModifierFactory {
    @Override
    public Modifier build(Element parent) {
        return new Modifier("Twist", "twist($${point})", parent);
    }
}
