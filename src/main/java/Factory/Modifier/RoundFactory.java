package Factory.Modifier;

import Factory.Abstract.ModifierFactory;
import Model.Element.Element;
import Model.Element.Modifier;

public class RoundFactory extends ModifierFactory {
    @Override
    public Modifier build(Element parent) {
        return new Modifier("Round", "round(${func}, 0.005)", parent);
    }
}
