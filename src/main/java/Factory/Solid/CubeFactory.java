package Factory.Solid;

import Factory.Abstract.SolidFactory;
import Model.Element.Element;
import Model.Element.Solid;
import Model.Helper.Float;
import Model.Helper.Parameter;
import Model.Helper.Vec3;
import Service.MatrixParser;

import java.util.ArrayList;
import java.util.List;

public class CubeFactory extends SolidFactory {
    @Override
    public Solid build(Element parent) {
        List<Parameter> parameters = new ArrayList<>();
        Parameter origin = new Vec3("origin", 0, 0, 0);
        parameters.add(origin);
        Parameter size = new Float("size", 1);
        parameters.add(size);
        String function = String.format("getCube(${point}, %s, %s)", origin.toShader(), size.toShader());
        return new Solid("Cube", function, parameters, parent);
    }
}
