package Factory.Solid;

import Factory.Abstract.SolidFactory;
import Model.Element.Element;
import Model.Element.Solid;

import java.util.ArrayList;

public class VoidFactory extends SolidFactory {
    @Override
    public Solid build(Element parent) {
        String function = "999999999.";
        return new Solid("Void", function, new ArrayList<>(), parent);
    }
}
