package Factory.Solid;

import Factory.Abstract.SolidFactory;
import Model.Element.Element;
import Model.Element.Solid;
import Model.Helper.Float;
import Model.Helper.Parameter;
import Model.Helper.Vec3;

import java.util.ArrayList;
import java.util.List;

public class ConeFactory extends SolidFactory {
    @Override
    public Solid build(Element parent) {
        List<Parameter> parameters = new ArrayList<>();
        String function = String.format("getCone(${point}, vec2(0.5, 0.5), 1.0)");
        return new Solid("Cone", function, parameters, parent);
    }
}
