package Factory.Solid;

import Factory.Abstract.SolidFactory;
import Model.Element.Element;
import Model.Element.Solid;
import Model.Helper.Float;
import Model.Helper.Parameter;
import Model.Helper.Vec3;

import java.util.ArrayList;
import java.util.List;

public class CylinderFactory extends SolidFactory {
    @Override
    public Solid build(Element parent) {
        List<Parameter> parameters = new ArrayList<>();
        Parameter a = new Vec3("a", 0, 0, 0);
        Parameter b = new Vec3("b", 0, 1, 0);
        Parameter r = new Float("r", 1);

        return new Solid("Cylinder", String.format("getCylinder(${point}, %s, %s, %s)", a.toShader(), b.toShader(), r.toShader() ), parameters, parent);
    }
}
