package Factory.Solid;

import Factory.Abstract.SolidFactory;
import Model.Element.Element;
import Model.Element.Solid;
import Model.Helper.Float;
import Model.Helper.Parameter;
import Model.Helper.Vec3;

import java.util.ArrayList;
import java.util.List;

public class SphereFactory extends SolidFactory {
    @Override
    public Solid build(Element parent) {
        List<Parameter> parameters = new ArrayList<>();
        Parameter origin = new Vec3("origin", 0, 0, 0);
        Parameter radius = new Float("radius", 1);
        parameters.add(origin);
        parameters.add(radius);
        String function = String.format("getSphere(${point}, %s, %s)", origin.toShader(), radius.toShader());
        Solid sphere = new Solid("Sphere", function, parameters, parent);
        sphere.setTranslationy(0.5f);
        sphere.setTranslationz(0.5f);
        return sphere;
    }
}
