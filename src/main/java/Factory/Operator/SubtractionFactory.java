package Factory.Operator;

import Factory.Abstract.OperatorFactory;
import Model.Element.Element;
import Model.Element.Operator;

public class SubtractionFactory extends OperatorFactory {
    @Override
    public Operator build(Element parent) {
        return new Operator("Subtraction", "max(-(%s), %s)", parent);
    }
}
