package Factory.Operator;

import Factory.Abstract.OperatorFactory;
import Model.Element.Element;
import Model.Element.Operator;

public class AdditionFactory extends OperatorFactory {
    @Override
    public Operator build(Element parent) {
        return new Operator("Addition", "min(%s, %s)", parent);
    }
}
