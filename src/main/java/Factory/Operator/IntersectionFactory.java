package Factory.Operator;

import Factory.Abstract.OperatorFactory;
import Model.Element.Element;
import Model.Element.Operator;

public class IntersectionFactory extends OperatorFactory {
    @Override
    public Operator build(Element parent) {
        return new Operator("Intersection", "max(%s, %s)", parent);
    }
}
