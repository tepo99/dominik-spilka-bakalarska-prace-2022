import Controller.RenderController;
import View.Viewport.RenderWindow;

public class App {

    public static void main(String[] args) {
        RenderController renderController = new RenderController();
        RenderWindow renderWindow = new RenderWindow(renderController);
        Thread renderThread = new Thread(renderWindow);
        renderThread.start();
    }

}
