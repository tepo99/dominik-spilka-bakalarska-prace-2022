package View.Components;

import Model.Element.Element;
import Model.Scene;
import Model.Subscribers.SceneSubscriber;
import Model.Subscribers.SolidsListPublisher;
import org.joml.Vector2f;
import org.joml.Vector4f;
import org.liquidengine.legui.style.Background;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SolidsList implements SolidsListPublisher, SceneSubscriber {

    private List<ListElement> listElements;
    private final Scene scene;

    public SolidsList(Scene scene) {
        this.scene = scene;
        scene.subscribe(this);
        buildList();
    }

    private void buildList() {
        listElements = new ArrayList<>();
        Element root = scene.getRoot();
        ListElement rootElement = new ListElement(scene, root, this, 0, 0);
        listElements.add(rootElement);
        getChildLabels(root, 1, 1);
        listElements.sort(Comparator.comparingInt(ListElement::getOrder));
        update();
    }

    private int getChildLabels(Element element, int indent, int order){
        for(Element e: element.getChildren()) {
            ListElement label = new ListElement(scene, e, this, indent, order);
            int newIndent = indent + 1;
            if(e.getChildren() != null) {
                order++;
                order = getChildLabels(e, newIndent, order);
            }
            order++;
            listElements.add(label);
        }
        order--;
        return order;
    }

    public List<ListElement> getListElements() {
        return listElements;
    }

    @Override
    public void react(){
        this.buildList();
        this.redrawSelected();
    }

    public void redrawSelected() {
        Background selectedBackground = new Background();
        selectedBackground.setColor(new Vector4f(0.7f, 0.7f, 0.1f, 1.0f));
        Background inactiveBackground = new Background();
        inactiveBackground.setColor(new Vector4f(0.7f, 0.7f, 0.7f, 1.0f));
        for(ListElement c: getListElements()){
            if(c.getElement() == scene.getSelected()) {
                c.getStyle().setBackground(selectedBackground);
            } else {
                c.getStyle().setBackground(inactiveBackground);
            }
        }
        update();
    }

    public void moveUp(ListElement element) {
        int index = listElements.indexOf(element);
        if(index <= 0) {
            return;
        }
        ListElement previous = listElements.get(index - 1);
        int indent = previous.getIndent();
        previous.setIndent(element.getIndent());
        element.setIndent(indent);
        element.setOrder(index - 1);
        previous.setOrder(index);
        listElements.set(index - 1, element);
        listElements.set(index, previous);
        scene.updateFromList(listElements);
        update();
    }

    public void moveDown(ListElement element) {
        int index = listElements.indexOf(element);
        if(index >= listElements.size() - 1 ) {
            return;
        }
        ListElement next = listElements.get(index + 1);
        int indent = next.getIndent();
        next.setIndent(element.getIndent());
        element.setIndent(indent);
        element.setOrder(index + 1);
        next.setOrder(index);
        listElements.set(index + 1, element);
        listElements.set(index, next);
        scene.updateFromList(listElements);
        update();
    }
}
