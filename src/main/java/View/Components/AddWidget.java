package View.Components;

import Factory.Modifier.BendFactory;
import Factory.Modifier.RoundFactory;
import Factory.Modifier.TwistFactory;
import Factory.Operator.AdditionFactory;
import Factory.Operator.IntersectionFactory;
import Factory.Operator.SubtractionFactory;
import Factory.Solid.ConeFactory;
import Factory.Solid.CubeFactory;
import Factory.Solid.CylinderFactory;
import Factory.Solid.SphereFactory;
import Model.Scene;
import org.liquidengine.legui.component.Button;
import org.liquidengine.legui.component.Widget;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;

public class AddWidget extends Widget {

    private final Scene scene;

    public AddWidget(Scene scene) {
        super();
        this.scene = scene;
        setSize(100, 300);
        setPosition(0, 80);
        createCubeButton();
        createSphereButton();
        createCylinderButton();
        createConeButton();
        createAdditionButton();
        createIntersectionButton();
        createSubtractionButton();
        createTwistButton();
        createBendButton();
        createRoundButton();
    }

    private void createCubeButton(){
        Button button = new Button("Cube");
        button.setSize(100, 20);
        button.setPosition(0, 0);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addSolid(new CubeFactory().build(null), scene.getSelected() != null ? scene.getSelected(): scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createSphereButton(){
        Button button = new Button("Sphere");
        button.setSize(100, 20);
        button.setPosition(0, 20);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addSolid(new SphereFactory().build(null), scene.getSelected() != null ? scene.getSelected(): scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createCylinderButton(){
        Button button = new Button("Cylinder");
        button.setSize(100, 20);
        button.setPosition(0, 40);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addSolid(new CylinderFactory().build(null), scene.getSelected() != null ? scene.getSelected(): scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createConeButton(){
        Button button = new Button("Cone");
        button.setSize(100, 20);
        button.setPosition(0, 60);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addSolid(new ConeFactory().build(null), scene.getSelected() != null ? scene.getSelected(): scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createAdditionButton(){
        Button button = new Button("Addition");
        button.setSize(100, 20);
        button.setPosition(0, 80);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addOperator(new AdditionFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createSubtractionButton(){
        Button button = new Button("Subtraction");
        button.setSize(100, 20);
        button.setPosition(0, 100);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addOperator(new SubtractionFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }
    private void createIntersectionButton(){
        Button button = new Button("Intersection");
        button.setSize(100, 20);
        button.setPosition(0, 120);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addOperator(new IntersectionFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }

    private void createTwistButton() {
        Button button = new Button("Twist");
        button.setSize(100, 20);
        button.setPosition(0, 140);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addModifier(new TwistFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }

    private void createBendButton() {
        Button button = new Button("Bend");
        button.setSize(100, 20);
        button.setPosition(0, 160);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addModifier(new BendFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }

    private void createRoundButton() {
        Button button = new Button("Round");
        button.setSize(100, 20);
        button.setPosition(0, 180);
        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.addModifier(new RoundFactory().build(null), scene.getSelected() != null ? scene.getSelected() : scene.getRoot());
            }
        });
        getContainer().add(button);
    }

}
