package View.Components;

import Model.Element.Element;
import Model.Scene;
import org.joml.Vector4f;
import org.liquidengine.legui.component.*;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.style.Background;

public class ListElement extends Panel {
    private Scene scene;
    private SolidsList parent;
    private Element element;
    private Button up, down, delete;
    private int indent = 0;
    private int order = 0;
    private boolean illegal = false;

    public ListElement(Scene scene, Element element, SolidsList parent, int indent, int order) {
        this.scene = scene;
        this.indent = indent;
        this.element = element;
        this.parent = parent;
        this.setSize(200, 20);
        this.order = order;
        this.setPosition(indent * 5, order * 20);
        this.add(new Label(element.getName(), 65, 0, 200, 20));
        up = new Button("U", 0, 0, 20 ,20);
        down = new Button("D", 20, 0, 20 ,20);
        delete = new Button("X", 40, 0, 20, 20);
        this.add(up);
        this.add(down);
        this.add(delete);
        initListeners();
        Background background = new Background();
        background.setColor(new Vector4f(0.7f, 0.7f, 0.7f, 1.0f));
        this.getStyle().setBackground(background);
    }

    private void initListeners() {
        MouseClickEventListener listener = event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.setSelected(element);
                parent.redrawSelected();
            }
        };
        this.getListenerMap().addListener(MouseClickEvent.class, listener);
        for(Component c: getChildComponents()) {
            if(!(c instanceof Button)) {
                c.getListenerMap().addListener(MouseClickEvent.class, listener);
            }
        }
        MouseClickEventListener upListener = event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                parent.moveUp(this);
            }
        };
        up.getListenerMap().addListener(MouseClickEvent.class, upListener);
        MouseClickEventListener downListener = event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                parent.moveDown(this);
            }
        };
        down.getListenerMap().addListener(MouseClickEvent.class, downListener);
        MouseClickEventListener deleteListener = event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                scene.deleteElement(element, scene.getRoot());
            }
        };
        delete.getListenerMap().addListener(MouseClickEvent.class, deleteListener);
    }

    public Element getElement() {
        return element;
    }

    public void setIndent(int indent) {
        this.indent = indent;
        this.setPosition(indent * 5, this.getPosition().y);
    }

    public int getIndent() {
        return indent;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
        this.setPosition(this.getPosition().x, order * 20);
    }

    public void setIllegal(boolean illegal) {
        this.illegal = illegal;
        Background background = new Background();
        if(illegal) {
            background.setColor(new Vector4f(0.7f, 0f, 0f, 1.0f));
        }
        else {
            background.setColor(new Vector4f(0.7f, 0.7f, 0.7f, 1.0f));
        }
        this.getStyle().setBackground(background);
    }
}
