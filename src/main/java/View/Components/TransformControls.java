package View.Components;

import EventListener.Subscribers.MouseEventSubscriber;
import Model.Element.Element;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.component.TextInput;
import org.liquidengine.legui.component.event.textinput.TextInputContentChangeEventListener;


public class TransformControls extends Panel implements MouseEventSubscriber {

    private final Element selected;
    private TextInput translX, translY, translZ, rotX, rotY, rotZ, scaleX, scaleY, scaleZ;

    public TransformControls(Element selected) {
        this.selected = selected;
        setSize(200, 300);
        Panel translation = buildTranslation();
        add(translation);
        Panel rotation = buildRotation();
        add(rotation);
        Panel scale = buildScale();
        add(scale);
    }

    private Panel buildTranslation(){
        Panel translation = new Panel();
        translation.setPosition(0, 0);
        translation.setSize(300, 40);
        translation.add(new Label("Translate", 0, 0, 300, 20));
        translX = new TextInput(Float.toString(selected.getTranslationx()), 0, 20, 60, 20);
        translY = new TextInput(Float.toString(selected.getTranslationy()), 70, 20, 60, 20);
        translZ = new TextInput(Float.toString(selected.getTranslationz()), 140, 20, 60, 20);
        TextInputContentChangeEventListener translXListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setTranslationx(newValue);
            } catch (NumberFormatException $e) {
                translX.getTextState().setText(event.getOldValue());
            }
        };
        translX.addTextInputContentChangeEventListener(translXListener);
        TextInputContentChangeEventListener translYListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setTranslationy(newValue);
            } catch (NumberFormatException $e) {
                translY.getTextState().setText(event.getOldValue());
            }
        };
        translY.addTextInputContentChangeEventListener(translYListener);
        TextInputContentChangeEventListener translZListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setTranslationz(newValue);
            } catch (NumberFormatException $e) {
                translZ.getTextState().setText(event.getOldValue());
            }
        };
        translZ.addTextInputContentChangeEventListener(translZListener);
        translation.add(translX);
        translation.add(translY);
        translation.add(translZ);
        return translation;
    }

    private Panel buildRotation(){
        Panel rotation = new Panel();
        rotation.setPosition(0, 40);
        rotation.setSize(300, 40);
        rotation.add(new Label("Rotate", 0, 0, 300, 20));
        rotX = new TextInput(Double.toString(Math.toDegrees(selected.getRotx())), 0, 20, 60, 20);
        rotY = new TextInput(Double.toString(Math.toDegrees(selected.getRoty())), 70, 20, 60, 20);
        rotZ = new TextInput(Double.toString(Math.toDegrees(selected.getRotz())), 140, 20, 60, 20);
        TextInputContentChangeEventListener rotXListener = event -> {
            try {
                float newValue = (float) Math.toRadians(Float.parseFloat(event.getNewValue()));
                selected.setRotx(newValue);
            } catch (NumberFormatException $e) {
                rotX.getTextState().setText(event.getOldValue());
            }
        };
        rotX.addTextInputContentChangeEventListener(rotXListener);
        TextInputContentChangeEventListener rotYListener = event -> {
            try {
                float newValue = (float) Math.toRadians(Float.parseFloat(event.getNewValue()));
                selected.setRoty(newValue);
            } catch (NumberFormatException $e) {
                rotY.getTextState().setText(event.getOldValue());
            }
        };
        rotY.addTextInputContentChangeEventListener(rotYListener);
        TextInputContentChangeEventListener rotZListener = event -> {
            try {
                float newValue = (float) Math.toDegrees(Float.parseFloat(event.getNewValue()));
                selected.setRotz(newValue);
            } catch (NumberFormatException $e) {
                rotZ.getTextState().setText(event.getOldValue());
            }
        };
        rotZ.addTextInputContentChangeEventListener(rotZListener);
        rotation.add(rotX);
        rotation.add(rotY);
        rotation.add(rotZ);
        return rotation;
    }

    private Panel buildScale(){
        Panel translation = new Panel();
        translation.setPosition(0, 80);
        translation.setSize(300, 40);
        translation.add(new Label("Scale", 0, 0, 300, 20));
        scaleX = new TextInput(Float.toString(selected.getScalex()), 0, 20, 60, 20);
        scaleY = new TextInput(Float.toString(selected.getScaley()), 70, 20, 60, 20);
        scaleZ = new TextInput(Float.toString(selected.getScalez()), 140, 20, 60, 20);
        TextInputContentChangeEventListener scaleXListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setScalex(newValue);
            } catch (NumberFormatException $e) {
                scaleX.getTextState().setText(event.getOldValue());
            }
        };
        scaleX.addTextInputContentChangeEventListener(scaleXListener);
        TextInputContentChangeEventListener scaleYListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setScaley(newValue);
            } catch (NumberFormatException $e) {
                scaleY.getTextState().setText(event.getOldValue());
            }
        };
        scaleY.addTextInputContentChangeEventListener(scaleYListener);
        TextInputContentChangeEventListener scaleZListener = event -> {
            try {
                float newValue = Float.parseFloat(event.getNewValue());
                selected.setScalez(newValue);
            } catch (NumberFormatException $e) {
                scaleZ.getTextState().setText(event.getOldValue());
            }
        };
        scaleZ.addTextInputContentChangeEventListener(scaleZListener);
        translation.add(scaleX);
        translation.add(scaleY);
        translation.add(scaleZ);
        return translation;
    }

    @Override
    public void react() {
        translX.getTextState().setText(Float.toString(selected.getTranslationx()));
        translY.getTextState().setText(Float.toString(selected.getTranslationy()));
        translZ.getTextState().setText(Float.toString(selected.getTranslationz()));
        rotX.getTextState().setText(Double.toString(Math.toDegrees(selected.getRotx())));
        rotY.getTextState().setText(Double.toString(Math.toDegrees(selected.getRoty())));
        rotZ.getTextState().setText(Double.toString(Math.toDegrees(selected.getRotz())));
        scaleX.getTextState().setText(Float.toString(selected.getScalex()));
        scaleY.getTextState().setText(Float.toString(selected.getScaley()));
        scaleZ.getTextState().setText(Float.toString(selected.getScalez()));
    }
}
