package View.Components;

import Model.Mode;
import Model.Scene;
import Model.Subscribers.SolidsListPublisher;
import Service.Computer;
import Service.Saving.XmlLoad;
import Service.Saving.XmlSave;
import org.joml.Vector4f;
import org.liquidengine.legui.component.Button;
import org.liquidengine.legui.component.Widget;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.style.Background;

import javax.swing.*;
import java.io.File;

public class Controls extends Widget implements SolidsListPublisher {

    private final Scene scene;
    private Button addMode, translationMode, rotationMode, scaleMode;
    private Button exportButton;

    public Controls(Scene scene) {
        this.scene = scene;
        setSize(260, 40);
        setPosition(0, 0);
        createAddModeButton();
        createMoveModeButton();
        createRotateModeButton();
        createScaleModeButton();
        createSaveButton();
        createLoadButton();
        createExportButton();
        highlightMode();
    }

    private void createAddModeButton(){
        addMode = new Button("A");
        addMode.setPosition(0, 0);
        addMode.setSize(20, 20);
        addMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK){
                scene.setMode(Mode.ADD);
                highlightMode();
            }
        });
        getContainer().add(addMode);
    }

    private void createMoveModeButton(){
        translationMode = new Button("M");
        translationMode.setPosition(20, 0);
        translationMode.setSize(20, 20);
        translationMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK){
                scene.setMode(Mode.MOVE);
                highlightMode();
            }
        });
        getContainer().add(translationMode);
    }
    private void createRotateModeButton(){
        rotationMode = new Button("R");
        rotationMode.setPosition(40, 0);
        rotationMode.setSize(20, 20);
        rotationMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK){
                scene.setMode(Mode.ROTATE);
                highlightMode();
            }
        });
        getContainer().add(rotationMode);
    }
    private void createScaleModeButton(){
        scaleMode = new Button("S");
        scaleMode.setPosition(60, 0);
        scaleMode.setSize(20, 20);
        scaleMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK){
                scene.setMode(Mode.SCALE);
                highlightMode();
            }
        });
        getContainer().add(scaleMode);
    }

    private void createSaveButton(){
        scaleMode = new Button("Save");
        scaleMode.setPosition(90, 0);
        scaleMode.setSize(60, 20);
        scaleMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK){
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Save...");
                int userSelection = fileChooser.showSaveDialog(null);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    XmlSave saver = new XmlSave();
                    try {
                        saver.saveScene(scene, fileChooser.getSelectedFile().getAbsolutePath());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                }
            }
        });
        getContainer().add(scaleMode);
    }

    private void createLoadButton(){
        scaleMode = new Button("Load");
        scaleMode.setPosition(150, 0);
        scaleMode.setSize(60, 20);
        scaleMode.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Load...");
                int userSelection = fileChooser.showOpenDialog(null);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    XmlLoad loader = new XmlLoad();
                    try {
                        loader.loadScene(scene, fileChooser.getSelectedFile().getAbsolutePath());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
        getContainer().add(scaleMode);
    }

    private void createExportButton() {
        exportButton = new Button("Export");
        exportButton.setPosition(210, 0);
        exportButton.setSize(60, 20);
        exportButton.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
            if(event.getAction() == MouseClickEvent.MouseClickAction.CLICK) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Export...");
                int userSelection = fileChooser.showSaveDialog(null);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    Computer computer = new Computer(scene);
                    try {
                        computer.initializeCustomFrame();
                        computer.renderCustomFrame(new File(fileChooser.getSelectedFile().getAbsolutePath()));
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
        getContainer().add(exportButton);
    }

    private void highlightMode(){
        Background basic = new Background();
        basic.setColor(new Vector4f(0.7f, 0.7f, 0.7f, 1f));
        Background highlighted = new Background();
        highlighted.setColor(new Vector4f(1, 1, 0, 1f));
        addMode.getStyle().setBackground(basic);
        translationMode.getStyle().setBackground(basic);
        rotationMode.getStyle().setBackground(basic);
        scaleMode.getStyle().setBackground(basic);
//        System.out.println(scene.getMode());
        switch (scene.getMode()) {
            case ADD -> addMode.getStyle().setBackground(highlighted);
            case MOVE -> translationMode.getStyle().setBackground(highlighted);
            case ROTATE -> rotationMode.getStyle().setBackground(highlighted);
            case SCALE -> scaleMode.getStyle().setBackground(highlighted);
        }
        update();
    }
}
