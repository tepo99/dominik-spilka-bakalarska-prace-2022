package View.Viewport;

import Controller.RenderController;
import org.lwjgl.Version;

import java.util.Objects;

import static org.lwjgl.glfw.GLFW.*;

public class RenderWindow implements Runnable {

    private RenderController renderController;

    public RenderWindow(RenderController renderController) {
        this.renderController = renderController;

    }

    @Override
    public void run() {
        try {
            System.out.println("Hello LWJGL " + Version.getVersion() + "!");
            renderController.start();

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            Objects.requireNonNull(glfwSetErrorCallback(null)).free();
        }
    }
}
